package id.go.kemenag.madrasah.pmrms.resources

import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.ResourcesService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@Suppress("UNCHECKED_CAST")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class ResourceServiceTest {

    @Autowired
    private lateinit var service: ResourcesService

    @Test
    @DisplayName("Test Datatable")
    fun test1() {
        println("Test Datatable")
        val test: ResponseEntity<ReturnData> = service.datatable(Pagination2Request())
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    @DisplayName("Test Detail")
    fun test2() {
        println("Test Detail")
        val test: ResponseEntity<ReturnData> = service.getDetail("123")
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

}
