package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_progress_report", schema = "public")
data class EventProgressReportEventId(
    @JsonIgnore
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
