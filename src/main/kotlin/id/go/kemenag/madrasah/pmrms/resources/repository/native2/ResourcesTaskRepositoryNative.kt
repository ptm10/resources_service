package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesTask
import id.go.kemenag.madrasah.pmrms.resources.pojo.Task
import org.springframework.stereotype.Repository

@Repository
class ResourcesTaskRepositoryNative : BaseRepositoryNative<ResourcesTask>(ResourcesTask::class.java)
