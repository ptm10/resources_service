package id.go.kemenag.madrasah.pmrms.resources.constant

const val POSITION_KETUA_PCU_PROV = "Ketua PCU"

const val POSITION_HEAD_PMU = "Project Management Unit Chair"

const val POSITION_SECRETARY_PMU = "Secretary PMU"

const val POSITION_TREASURER_PMU = "Treasurer PMU"

const val POSITION_COORDINATOR = "Coordinator"
