package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.HolidayManagement
import org.springframework.stereotype.Repository

@Repository
class HolidayManagementRepositoryNative : BaseRepositoryNative<HolidayManagement>(HolidayManagement::class.java)
