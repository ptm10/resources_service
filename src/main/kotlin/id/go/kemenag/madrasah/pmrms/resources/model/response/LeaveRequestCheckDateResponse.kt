package id.go.kemenag.madrasah.pmrms.resources.model.response

data class LeaveRequestCheckDateResponse(
    var worksDaysCount: Int = 0,
    var holidaysCount: Int = 0,
    var totalLeaveDays: Int = 0
)
