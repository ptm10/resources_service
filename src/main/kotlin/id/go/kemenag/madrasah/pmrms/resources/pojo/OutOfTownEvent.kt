package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "out_of_town_event", schema = "public")
data class OutOfTownEvent(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "resources_id")
    var resourcesId: String? = null,

    @ManyToOne
    @JoinColumn(name = "resources_id", insertable = false, updatable = false, nullable = true)
    var resources: Resources? = null,

    @Column(name = "event_id")
    var eventId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_id", insertable = false, updatable = false, nullable = true)
    var event: EventOutOfTown? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "days_length")
    var daysLength: Int? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "address")
    var address: String? = null,

    @Column(name = "transportation_id")
    var transportationId: String? = null,

    @ManyToOne
    @JoinColumn(name = "transportation_id", insertable = false, updatable = false, nullable = true)
    var transportation: Transportation? = null,

    @Column(name = "origin")
    var origin: String? = null,

    @Column(name = "destination")
    var destination: String? = null,

    @Column(name = "transportation_start_time")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+7")
    var transportationStartTime: Date? = null,

    @Column(name = "transportation_end_time")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+7")
    var transportationEndTime: Date? = null,

    @Column(name = "transportation_code")
    var transportationCode: String? = null,

    @Column(name = "approve_status")
    var approveStatus: Int? = 0,

    @OneToMany(mappedBy = "outOfTownEventId")
    @Where(clause = "active = true")
    var details: MutableList<OutOfTownEventResources>? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @Column(name = "rejected_message")
    var rejectedMessage: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
