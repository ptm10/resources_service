package id.go.kemenag.madrasah.pmrms.resources.model.request

import id.go.kemenag.madrasah.pmrms.resources.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class LeaveRequestRequest(

    @field:NotEmpty(message = "Tanggal Mulai $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "Tanggal Selesai $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null,

    @field:NotEmpty(message = "Id Tipe Cuti $VALIDATOR_MSG_REQUIRED")
    var leaveRequestTypeId: String? = null,

    var description: String? = null,

    @field:NotEmpty(message = "Alamat Selama Izin $VALIDATOR_MSG_REQUIRED")
    var address: String? = null,

    var resourcesReplacement: String? = null
)
