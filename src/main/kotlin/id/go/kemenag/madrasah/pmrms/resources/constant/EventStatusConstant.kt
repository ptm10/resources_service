package id.go.kemenag.madrasah.pmrms.resources.constant

const val EVENT_STATUS_PLANNING_REVISION = 1

const val EVENT_STATUS_EVELUATION_COORDINATOR_REVISION = 3

const val EVENT_STATUS_EVALUATION_TREASURER_REVISION = 5
