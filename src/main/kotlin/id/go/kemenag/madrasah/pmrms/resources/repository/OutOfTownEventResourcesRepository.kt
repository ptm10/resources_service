package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.OutOfTownEventResources
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.transaction.annotation.Transactional

interface OutOfTownEventResourcesRepository : JpaRepository<OutOfTownEventResources, String> {

    @Transactional
    fun deleteAllByOutOfTownEventId(outOfTownEventId: String?)

    fun getByResourcesIdAndActive(resourcesId: String?, active: Boolean = true): List<OutOfTownEventResources>
}
