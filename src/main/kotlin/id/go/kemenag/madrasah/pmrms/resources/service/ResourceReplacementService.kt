package id.go.kemenag.madrasah.pmrms.resources.service

import id.go.kemenag.madrasah.pmrms.resources.helpers.responseNotFound
import id.go.kemenag.madrasah.pmrms.resources.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.ResourcesReplacementRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class ResourceReplacementService {

    @Autowired
    private lateinit var repoNative: ResourcesReplacementRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getByTaskTypeTaskId(taskType: Int?, taskId: String?): ResponseEntity<ReturnData> {
        return try {
            val reqDatatable = Pagination2Request()
            reqDatatable.enablePage = true
            reqDatatable.size = 1
            reqDatatable.paramIs?.add(ParamSearch("taskType", "int", taskType.toString()))
            reqDatatable.paramIs?.add(ParamSearch("taskId", "string", taskId))

            val datas = repoNative.getPage(reqDatatable)
            if (!datas?.content.isNullOrEmpty()) {
                responseSuccess(data = datas?.content?.get(0))
            } else {
                responseNotFound()
            }
        } catch (e: Exception) {
            throw e
        }
    }
}
