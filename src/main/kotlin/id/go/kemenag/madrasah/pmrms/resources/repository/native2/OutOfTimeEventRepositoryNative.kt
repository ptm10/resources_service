package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.OutOfTownEvent
import org.springframework.stereotype.Repository

@Repository
class OutOfTimeEventRepositoryNative : BaseRepositoryNative<OutOfTownEvent>(OutOfTownEvent::class.java)
