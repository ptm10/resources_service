package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskResourcesTag
import id.go.kemenag.madrasah.pmrms.resources.service.TaskResourcesTagService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Api(tags = ["Task Resources Tag"], description = "Task Resources Tag API")
@RestController
@RequestMapping(path = ["task-resources-tag"])
class TaskResourcesTagController {

    @Autowired
    private lateinit var service: TaskResourcesTagService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun save(@Valid @RequestBody request: TaskResourcesTag): ResponseEntity<ReturnData> {
        return service.save(request)
    }
}
