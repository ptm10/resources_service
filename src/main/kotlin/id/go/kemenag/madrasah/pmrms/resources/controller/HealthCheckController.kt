package id.go.kemenag.madrasah.pmrms.resources.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(path = ["health-check"])
class HealthCheckController {

    @GetMapping(value = [""])
    fun check(): Int {
        return 1
    }
}
