package id.go.kemenag.madrasah.pmrms.resources.constant

const val TASK_TYPE_CHANGE_ROLE_REQUEST = 1

const val TASK_TYPE_LEAVE_REQUEST = 2

const val TASK_TYPE_OUT_OF_TOWN_REQUEST = 3

const val TASK_TYPE_RESOURCES_TASK = 4

const val TASK_TYPE_AWP_IMPLEMENTATION = 5

const val TASK_TYPE_EVENT = 6

const val TASK_TYPE_EVENT_PROGRESS_REPORT = 7

const val TASK_TYPE_EVENT_REPORT = 8

const val TASK_TYPE_AWP_IMPLEMENTATION_REPORT = 9

const val TASK_STATUS_NEW = 0

const val TASK_STATUS_ON_PROGRESS = 1

const val TASK_STATUS_APPROVED = 2

const val TASK_STATUS_REJECT = 3

const val TASK_STATUS_DONE_BY_RECEIVER = 4

val TASK_TITTLES = mapOf(
    TASK_TYPE_LEAVE_REQUEST to "Izin/Cuti",
    TASK_TYPE_OUT_OF_TOWN_REQUEST to "Kegiatan di Luar Kota",
    TASK_TYPE_RESOURCES_TASK to "Memberikan Tugas"
)

const val TASK_NUMBER_TASK = 0

const val TASK_NUMBER_TASK_REVISION = 1
