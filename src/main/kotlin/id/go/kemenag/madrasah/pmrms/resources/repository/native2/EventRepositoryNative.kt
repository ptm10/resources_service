package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.Event
import org.springframework.stereotype.Repository

@Repository
class EventRepositoryNative : BaseRepositoryNative<Event>(Event::class.java)
