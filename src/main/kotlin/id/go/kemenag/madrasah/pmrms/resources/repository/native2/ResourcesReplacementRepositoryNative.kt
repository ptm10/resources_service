package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesReplacement
import org.springframework.stereotype.Repository

@Repository
class ResourcesReplacementRepositoryNative :
    BaseRepositoryNative<ResourcesReplacement>(ResourcesReplacement::class.java)
