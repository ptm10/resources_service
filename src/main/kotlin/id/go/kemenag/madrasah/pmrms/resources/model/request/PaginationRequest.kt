package id.go.kemenag.madrasah.pmrms.resources.model.request

import io.swagger.annotations.ApiModelProperty

class PaginationRequest(

    @ApiModelProperty(
        value = "1 enable or 0 disable, default 1",
        name = "enablePage",
        dataType = "Intreger",
        example = "1"
    )
    var enablePage: Int? = 1,

    @ApiModelProperty(
        value = "0 or 1, default 1",
        name = "page",
        dataType = "Intreger",
        example = "0"
    )
    var page: Int? = 0,

    @ApiModelProperty(
        value = "10 or 15, default 10",
        name = "size",
        dataType = "Intreger",
        example = "10"
    )
    var size: Int? = 10,

    @ApiModelProperty(
        value = "desc or asc, default desc",
        name = "sort",
        dataType = "Intreger",
        example = "10"
    )
    var sort: String? = "desc",

    @ApiModelProperty(
        value = "any, default updatedAt",
        name = "sortBy",
        dataType = "String",
        example = "updatedAt"
    )
    var sortBy: String? = "updatedAt",

    @ApiModelProperty(
        value = "any, default empty string",
        name = "param",
        dataType = "String"
    )
    var param: String? = ""

)
