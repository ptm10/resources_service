package id.go.kemenag.madrasah.pmrms.resources.model.request


class SupervisiorByComponentRoleRequest(

    var componentId: String? = null,

    var userId: String? = null,

    var roleIds: List<String>? = null
)
