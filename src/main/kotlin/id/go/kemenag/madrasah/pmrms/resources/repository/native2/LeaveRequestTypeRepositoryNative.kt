package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.LeaveRequestType
import org.springframework.stereotype.Repository

@Repository
class LeaveRequestTypeRepositoryNative : BaseRepositoryNative<LeaveRequestType>(LeaveRequestType::class.java)
