package id.go.kemenag.madrasah.pmrms.resources.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.constant.HEADER_STRING
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnDataFiles
import kong.unirest.Unirest

@Suppress("UNCHECKED_CAST")
class RequestHelpers {

    companion object {

        fun authDetail(authUrl: String, bearer: String): ReturnData? {
            return try {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$authUrl/auth/detail"
                val response = Unirest.get(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .asString()

                objectMapper.readValue(response.body, ReturnData::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        fun filesGetById(id: String, baseUrl: String, bearer: String): ReturnDataFiles {
            val rData = ReturnDataFiles()
            rData.message = "Gagal melakukan request File"
            try {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$baseUrl/files/get-by-id?id=$id"
                val response = Unirest.get(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .asString()

                val responseData: ReturnDataFiles? = objectMapper.readValue(response.body, ReturnDataFiles::class.java)

                responseData?.let {
                    it.success?.let { s ->
                        if (s) {
                            rData.success = true
                            rData.message = ""
                            rData.data = it.data
                        } else {
                            if (!rData.message.isNullOrEmpty()) {
                                rData.message = it.message
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return rData
        }
    }
}
