package id.go.kemenag.madrasah.pmrms.resources.model.users

data class Users(

    var id: String? = null,

    var firstName: String? = null,

    var lastName: String? = null,

    var email: String? = null,

    var profilePictureId: String? = null,

    var componentId: String? = null,

    var component: Component? = null,

    var provinceId: String? = null,

    var province: Province? = null,

    var roles: MutableSet<UsersRole>? = null,

    var resourcesId: String? = null,

    var resources: UsersResources? = null
)
