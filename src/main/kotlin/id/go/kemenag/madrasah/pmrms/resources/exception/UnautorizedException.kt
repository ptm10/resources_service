package id.go.kemenag.madrasah.pmrms.resources.exception

import org.springframework.http.HttpStatus

/**
 *
 * response status code indicates that the server cannot or will not process
 * the request due to something that is perceived to be a client error
 * (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).
 */
class UnautorizedException : BaseException {

    constructor(message: String? = "Unautorized") {
        this.code = "400"
        this.message = message!!
        this.status = HttpStatus.UNAUTHORIZED
    }

    constructor(message: String? = "Unautorized", data: Any?) {
        this.code = "400"
        this.message = message!!
        this.status = HttpStatus.UNAUTHORIZED
        this.data = data
    }

}
