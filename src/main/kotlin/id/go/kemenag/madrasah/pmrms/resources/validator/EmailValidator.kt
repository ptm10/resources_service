package id.go.kemenag.madrasah.pmrms.resources.validator

import id.go.kemenag.madrasah.pmrms.resources.constant.EMAIL_VALID_REGEX
import id.go.kemenag.madrasah.pmrms.resources.constant.VALIDATOR_MSG_NOT_VALID
import org.apache.commons.validator.routines.EmailValidator
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD)
@MustBeDocumented
@Constraint(validatedBy = [CustomEmailValidatorConstrain::class])
annotation class EmailValidator(
    val message: String = "Email $VALIDATOR_MSG_NOT_VALID",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)

class CustomEmailValidatorConstrain :
    ConstraintValidator<id.go.kemenag.madrasah.pmrms.resources.validator.EmailValidator, String> {
    override fun isValid(p0: String?, p1: ConstraintValidatorContext?): Boolean {
        if (p0.isNullOrEmpty()) {
            return true
        } else {
            if (!p0.contains(EMAIL_VALID_REGEX)) {
                return false
            }
        }
        return EmailValidator.getInstance().isValid(p0)
    }
}
