package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.UsersRole
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface UserRoleRepository : JpaRepository<UsersRole, String> {

    @Query("select ur.userId from UsersRole ur where ur.roleId in(:rolesId) and ur.active = true")
    fun getUserIdsByRoleIds(rolesId: List<String>): List<String>

    @Query("select ur.userId from UsersRole ur where ur.roleId in(:rolesId) and ur.active = true GROUP BY ur.userId having count(distinct ur.roleId) = :length")
    fun getUserIdsByRoleIds(rolesId: List<String>, length: Long): List<String>

    @Query("select ur.userId from UsersRole ur where ur.roleId in(:roleIds) and ur.active = true and ur.user.componentId = :componentId and ur.user.active = true")
    fun getUserIdsByComponentIdRoleIds(componentId: String?, roleIds: List<String>?): List<String>
}
