package id.go.kemenag.madrasah.pmrms.resources.model

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class TaskNotificationUpdateByTaskRequest(
    @field:NotEmpty
    var taskId: String? = null,
    @field:NotNull
    var taskType: Int? = null
)
