package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "resources_replacement", schema = "public")
data class ResourcesReplacement(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "resources_id")
    var resourcesId: String? = null,

    @ManyToOne
    @JoinColumn(name = "resources_id", insertable = false, updatable = false, nullable = true)
    var resources: Resources? = null,

    @Column(name = "resources_replace_id")
    var resourcesReplaceId: String? = null,

    @ManyToOne
    @JoinColumn(name = "resources_replace_id", insertable = false, updatable = false, nullable = true)
    var resourcesReplace: Resources? = null,

    @Column(name = "task_type")
    var taskType: Int? = null,

    @Column(name = "task_id")
    var taskId: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "approved")
    var approved: Boolean? = false,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
