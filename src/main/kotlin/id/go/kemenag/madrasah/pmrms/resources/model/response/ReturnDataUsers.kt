package id.go.kemenag.madrasah.pmrms.resources.model.response

import id.go.kemenag.madrasah.pmrms.resources.pojo.Users


data class ReturnDataUsers(
    var success: Boolean? = false,
    var data: Users? = null,
    var message: String? = null
)
