package id.go.kemenag.madrasah.pmrms.resources.model.request

import javax.validation.constraints.NotNull

data class TaskNotificationUpdateRequest(
    @field:NotNull
    var readed: Boolean? = null
)
