package id.go.kemenag.madrasah.pmrms.resources.model.request

import id.go.kemenag.madrasah.pmrms.resources.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class OutOfTownRequest(

    @field:NotEmpty(message = "Tanggal Mulai $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "Tanggal Selesai $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null,

    @field:NotEmpty(message = "Kegitatan $VALIDATOR_MSG_REQUIRED")
    var eventId: String? = null,

    var description: String? = null,

    @field:NotEmpty(message = "Alamat Kegiatan $VALIDATOR_MSG_REQUIRED")
    var address: String? = null,

    @field:NotEmpty(message = "Moda Transportasi $VALIDATOR_MSG_REQUIRED")
    var transportationId: String? = null,

    @field:NotEmpty(message = "Dari (tempat asal) $VALIDATOR_MSG_REQUIRED")
    var origin: String? = null,

    @field:NotEmpty(message = "Ke (tempat tujuan) $VALIDATOR_MSG_REQUIRED")
    var destination: String? = null,

    @field:NotEmpty(message = "Jadwal Mulai Transportasi $VALIDATOR_MSG_REQUIRED")
    var transportationStartTime: String? = null,

    @field:NotEmpty(message = "Jadwal Selesai Transportasi $VALIDATOR_MSG_REQUIRED")
    var transportationEndTime: String? = null,

    var transportationCode: String? = null,

    var resourcesIds: MutableList<String> = mutableListOf()

)
