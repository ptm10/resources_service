package id.go.kemenag.madrasah.pmrms.resources.service

import id.go.kemenag.madrasah.pmrms.resources.constant.ROLE_ID_ADMINSTRATOR
import id.go.kemenag.madrasah.pmrms.resources.helpers.getUserLogin
import id.go.kemenag.madrasah.pmrms.resources.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.resources.helpers.userHasRoles
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.repository.ComponentRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.IriRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class IriService {

    @Autowired
    private lateinit var repoNative: IriRepositoryNative

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                val componentCode: String? = getUserLogin()?.component?.code?.substringBefore(".")
                if (componentCode != null) {
                    req.paramIs?.add(ParamSearch("code", "string", componentCode))
                } else {
                    req.paramIs?.add(ParamSearch("code", "string", System.currentTimeMillis().toString()))
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getByComponentId(id: String?): ResponseEntity<ReturnData> {
        return try {
            val req = Pagination2Request()
            val componentCode: String? = repoComponent.findByIdAndActive(id).get().code?.substringBefore(".")
            if (componentCode != null) {
                req.paramIs?.add(ParamSearch("code", "string", componentCode))
            } else {
                req.paramIs?.add(ParamSearch("code", "string", System.currentTimeMillis().toString()))
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
