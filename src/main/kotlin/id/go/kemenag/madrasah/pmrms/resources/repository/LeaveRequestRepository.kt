package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.LeaveRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface LeaveRequestRepository : JpaRepository<LeaveRequest, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<LeaveRequest>

    @Query("from LeaveRequest where resourcesId = :resourcesId and ((:startDate between startDate and endDate) or (:endDate between startDate and endDate)) and active = true and approveStatus in (:approveStatus)")
    fun findByResourcesIdStartDateAndDate(
        @Param("resourcesId") resourcesId: String?,
        @Param("startDate") startDate: Date?,
        @Param("endDate") endDate: Date?,
        @Param("approveStatus") approveStatus: List<Int>?
    ): Optional<LeaveRequest>
}
