package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.AwpImplementation
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AwpImplementationRepository : JpaRepository<AwpImplementation, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<AwpImplementation>
}
