package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskNotification
import org.springframework.stereotype.Repository

@Repository
class TaskNotificationRepositoryNative : BaseRepositoryNative<TaskNotification>(TaskNotification::class.java)
