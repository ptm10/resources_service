package id.go.kemenag.madrasah.pmrms.resources.config

import id.go.kemenag.madrasah.pmrms.resources.constant.AUDIENCE_FILTER_PATH
import id.go.kemenag.madrasah.pmrms.resources.interceptor.TokenInterceptor
import org.springframework.boot.web.server.ErrorPage
import org.springframework.boot.web.server.ErrorPageRegistrar
import org.springframework.boot.web.server.ErrorPageRegistry
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.web.firewall.RequestRejectedException
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.util.*
import javax.servlet.SessionTrackingMode


@Configuration
class SpringWebConfig : WebMvcConfigurer {

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
            .allowedOrigins(
                "http://localhost:4200",
                "http://pmrms.greatpmo.com",
                "http://pmrms-dev.greatpmo.com",
                "http://pmrms-pre-prod.greatpmo.com"
            )
            .allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS", "PUT", "TRACE")
            .allowedHeaders("*")
            .maxAge(60000)
    }

    @Bean
    fun tokenFilter(): FilterRegistrationBean<TokenInterceptor>? {
        val registrationBean: FilterRegistrationBean<TokenInterceptor> =
            FilterRegistrationBean<TokenInterceptor>()

        AUDIENCE_FILTER_PATH.forEach {
            it.value.forEach {
                registrationBean.addUrlPatterns(it)
            }
        }

        registrationBean.filter = TokenInterceptor()
        registrationBean.order = 1
        return registrationBean
    }

    @Bean
    fun securityErrorPageRegistrar(): ErrorPageRegistrar? {
        return ErrorPageRegistrar { registry: ErrorPageRegistry ->
            registry.addErrorPages(
                ErrorPage(
                    RequestRejectedException::class.java, "/errors/400"
                )
            )
        }
    }

    @Bean
    fun servletContextInitializer(): ServletContextInitializer? {
        return ServletContextInitializer { servletContext ->
            servletContext.setSessionTrackingModes(Collections.singleton(SessionTrackingMode.COOKIE))
            val sessionCookieConfig = servletContext.sessionCookieConfig
            sessionCookieConfig.isHttpOnly = true
        }
    }

    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addRedirectViewController("/v2/api-docs", "/v2/api-docs")
        registry.addRedirectViewController("/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui")
        registry.addRedirectViewController(
            "/swagger-resources/configuration/security",
            "/swagger-resources/configuration/security"
        )
        registry.addRedirectViewController("/swagger-resources", "/swagger-resources")
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("/swagger-ui.html**")
            .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html")
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/")
    }
}
