package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.TaskNotificationUpdateByTaskRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.TaskNotificationUpdateRequest
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.TaskNotificationService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Task Notification"], description = "Task Notification API")
@RestController
@RequestMapping(path = ["task-notification"])
class TaskNotificationController {

    @Autowired
    private lateinit var service: TaskNotificationService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun updateData(
        @PathVariable id: String,
        @Valid @RequestBody request: TaskNotificationUpdateRequest
    ): ResponseEntity<*>? {
        return service.updateData(id, request)
    }

    @PutMapping(value = ["update-by-task-id-task-type"], produces = ["application/json"])
    fun updateByTaskIdTaskType(
        @Valid @RequestBody request: TaskNotificationUpdateByTaskRequest
    ): ResponseEntity<*>? {
        return service.updateByTaskIdTaskType(request)
    }
}
