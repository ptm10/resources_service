package id.go.kemenag.madrasah.pmrms.resources.model.request


class UserSaveRequest(

    var email: String? = null,

    var firstName: String? = null,

    var lastName: String? = null,

    var profilePictureId: String? = null,

    var roles: List<String>? = null
)
