package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.Pdo
import org.springframework.stereotype.Repository

@Repository
class PdoRepositoryNative : BaseRepositoryNative<Pdo>(Pdo::class.java)
