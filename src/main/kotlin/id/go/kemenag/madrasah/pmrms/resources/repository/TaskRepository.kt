package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Task
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TaskRepository : JpaRepository<Task, String> {

    fun findByTaskIdAndTaskTypeAndActive(taskId: String?, taskType: Int?, active: Boolean? = true): Optional<Task>

    fun findByTaskIdAndTaskTypeAndCreatedForAndActive(
        taskId: String?,
        taskType: Int?,
        createdFor: String?,
        active: Boolean? = true
    ): Optional<Task>

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Task>


    fun findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
        taskId: String?,
        taskType: Int?,
        createdFor: String?,
        taskNumber: Int?,
        active: Boolean? = true
    ): Optional<Task>
}
