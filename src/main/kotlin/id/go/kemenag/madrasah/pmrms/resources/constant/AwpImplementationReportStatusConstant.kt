package id.go.kemenag.madrasah.pmrms.resources.constant

const val AWP_IMPLEMENTATION_REPORT_STATUS_NEW = 0

const val AWP_IMPLEMENTATION_REPORT_STATUS_REVISION = 1

const val AWP_IMPLEMENTATION_REPORT_STATUS_APPROVED = 2
