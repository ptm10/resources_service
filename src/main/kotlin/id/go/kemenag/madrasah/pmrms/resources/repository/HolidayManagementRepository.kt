package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.HolidayManagement
import org.springframework.data.jpa.repository.JpaRepository

interface HolidayManagementRepository : JpaRepository<HolidayManagement, String> {

    fun getByActive(active: Boolean? = true) : List<HolidayManagement>
}
