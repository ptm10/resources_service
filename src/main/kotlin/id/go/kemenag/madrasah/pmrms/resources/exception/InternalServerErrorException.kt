package id.go.kemenag.madrasah.pmrms.resources.exception

import org.springframework.http.HttpStatus

/**
 * The 500 (Internal Server Error) status code indicates that the server
 * encountered an unexpected condition that prevented it from fulfilling
 * the request.
 */
class InternalServerErrorException : BaseException {

    constructor() {
        this.code = "500"
        this.message = "Internal Server Error"
        this.status = HttpStatus.INTERNAL_SERVER_ERROR
    }

    constructor(message: String?) {
        this.code = "500"
        this.message = message!!
        this.status = HttpStatus.INTERNAL_SERVER_ERROR
    }

}
