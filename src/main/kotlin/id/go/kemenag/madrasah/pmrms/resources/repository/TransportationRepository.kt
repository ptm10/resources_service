package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Transportation
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TransportationRepository : JpaRepository<Transportation, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Transportation>

}
