package id.go.kemenag.madrasah.pmrms.resources.model.request

import id.go.kemenag.madrasah.pmrms.resources.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class ResourcesRequest(
    @field:NotEmpty(message = "Id User $VALIDATOR_MSG_REQUIRED")
    var userId: String? = null,

    @field:NotEmpty(message = "Unit Id $VALIDATOR_MSG_REQUIRED")
    var unitId: String? = null,

    @field:NotEmpty(message = "Position $VALIDATOR_MSG_REQUIRED")
    var position: String? = null,

    @field:NotEmpty(message = "Position $VALIDATOR_MSG_REQUIRED")
    var phoneNumber: String? = null,

    var supervisiorId: String? = null
)
