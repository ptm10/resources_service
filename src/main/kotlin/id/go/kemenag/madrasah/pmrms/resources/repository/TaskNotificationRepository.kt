package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskNotification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface TaskNotificationRepository : JpaRepository<TaskNotification, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<TaskNotification>

    fun getByTaskIdAndTaskTypeAndActive(
        taskId: String?,
        taskType: Int?,
        active: Boolean? = true
    ): List<TaskNotification>

    fun getByTaskIdAndTaskTypeAndUserIdAndActive(
        taskId: String?,
        taskType: Int?,
        userId: String?,
        active: Boolean? = true
    ): List<TaskNotification>
}
