package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesReplacement
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface ResourcesReplacementRepository : JpaRepository<ResourcesReplacement, String> {

    @Query("from ResourcesReplacement where resourcesId = :resourcesId and :date between startDate and endDate and active = true and approved = :approved order by updatedAt desc")
    fun findByResourcesIdDateApproved(
        @Param("resourcesId") resourcesId: String?,
        @Param("date") date: Date?,
        @Param("approved") approved: Boolean? = true,
    ): List<ResourcesReplacement>

    fun getByTaskIdAndTaskTypeAndActive(
        taskId: String?,
        taskType: Int?,
        active: Boolean? = true
    ): List<ResourcesReplacement>


    @Query(
        "from ResourcesReplacement where resourcesReplaceId = :resourcesReplaceId " +
                "and ((startDate between :startDate and :endDate) or (endDate between :startDate and :endDate)) " +
                "and taskId = :taskId " +
                "and taskType = :taskType " +
                "and active = true " +
                "and approved = :approved " +
                "order by updatedAt desc"
    )
    fun getByResourcesReplaceIdDateTaskIdTaskTypeApproved(
        resourcesReplaceId: String?,
        startDate: Date?,
        endDate: Date?,
        taskId: String?,
        taskType: Int?,
        approved: Boolean? = true
    ): List<ResourcesReplacement>
}
