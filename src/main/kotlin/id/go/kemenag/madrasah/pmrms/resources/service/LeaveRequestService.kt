package id.go.kemenag.madrasah.pmrms.resources.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.constant.*
import id.go.kemenag.madrasah.pmrms.resources.helpers.*
import id.go.kemenag.madrasah.pmrms.resources.model.request.*
import id.go.kemenag.madrasah.pmrms.resources.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.resources.model.response.LeaveRequestCheckDateResponse
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.model.users.UsersResources
import id.go.kemenag.madrasah.pmrms.resources.pojo.LeaveRequest
import id.go.kemenag.madrasah.pmrms.resources.pojo.LeaveRequestType
import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesReplacement
import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesSchedule
import id.go.kemenag.madrasah.pmrms.resources.repository.*
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.LeaveRequestRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST")
@Service
class LeaveRequestService {

    @Autowired
    private lateinit var repo: LeaveRequestRepository

    @Autowired
    private lateinit var repoNative: LeaveRequestRepositoryNative

    @Autowired
    private lateinit var repoLeaveRequestType: LeaveRequestTypeRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoResourcesReplacement: ResourcesReplacementRepository

    @Autowired
    private lateinit var repoHolidayManagement: HolidayManagementRepository

    @Autowired
    private lateinit var serviceTask: TaskService

    @Autowired
    private lateinit var serviceResourcesSchedule: ResourcesScheduleService

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var logActivityService: LogActivityService


    fun datatableSupervisior(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                val supervisior = repoResources.findByUserIdAndActive(getUserLogin()?.id)
                if (supervisior.isPresent) {
                    val supervisiorIds = mutableListOf<String>()
                    supervisiorIds.add(supervisior.get().id ?: "")

                    // add task from replacement
                    val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                    repoResourcesReplacement.findByResourcesIdDateApproved(
                        supervisior.get().id, dform.parse(dform.format(Date()))
                    ).forEach {
                        if (!supervisiorIds.contains(it.resourcesReplaceId)) {
                            supervisiorIds.add(it.resourcesReplaceId ?: "")
                        }
                    }

                    req.paramIn?.add(ParamArray("resources.supervisiorId", "string", supervisiorIds))
                } else {
                    req.paramIs?.add(
                        ParamSearch(
                            "resources.supervisiorId",
                            "string",
                            System.currentTimeMillis().toString()
                        )
                    )
                }
            } else {
                req.paramNotIn?.add(ParamArray("resources.userId", "string", listOf(getUserLogin()?.id ?: "")))
            }

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableStaff(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val staff = repoResources.findByUserIdAndActive(getUserLogin()?.id)
            if (staff.isPresent) {
                req.paramIs?.add(ParamSearch("resourcesId", "string", staff.get().id))
            } else {
                req.paramIs?.add(
                    ParamSearch(
                        "resourcesId",
                        "string",
                        System.currentTimeMillis().toString()
                    )
                )
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveData(request: LeaveRequestRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(request)
    }

    fun updateData(id: String, request: LeaveRequestRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            if (update.get().approveStatus == APPROVAL_STATUS_REQUEST) {
                saveOrUpdate(request, update.get())
            } else {
                responseBadRequest(VALIDATOR_MSG_CANNOT_CHANGE_DATA)
            }
        } else {
            responseNotFound()
        }
    }

    fun delete(request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return try {
            val data = repo.findByIdAndActive(request.id)
            if (data.isPresent) {
                data.get().active = false
                data.get().updatedAt = Date()
                repo.save(data.get())

                serviceTask.deleteFromTask(data.get().id ?: "", TASK_TYPE_LEAVE_REQUEST)
                serviceResourcesSchedule.deleteFromTask(data.get().resourcesId, data.get().id, TASK_TYPE_LEAVE_REQUEST)

                responseSuccess()
            } else {
                responseNotFound()
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveOrUpdate(request: LeaveRequestRequest, update: LeaveRequest? = null): ResponseEntity<ReturnData> {
        try {
            val resources: UsersResources =
                getUserLogin()?.resources ?: return responseBadRequest(message = "User tidak terdaftar sebagai staff")

            val validate = validate(request, resources)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val startDate: Date = validate["startDate"] as Date
            val endDate: Date = validate["endDate"] as Date
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            val checkResourcesLeave = repo.findByResourcesIdStartDateAndDate(
                resources.id, startDate, endDate, listOf(
                    APPROVAL_STATUS_REQUEST, APPROVAL_STATUS_APPROVE
                )
            )
            if (checkResourcesLeave.isPresent) {
                val messageResourceLeave =
                    "User sudah mengajukan izin ${checkResourcesLeave.get().leaveRequestType?.name} pada tanggal ${
                        dateFormat.format(
                            checkResourcesLeave.get().startDate
                        )
                    } sampai ${
                        dateFormat.format(
                            checkResourcesLeave.get().endDate
                        )
                    }"
                if (update == null) {
                    return responseBadRequest(
                        message = messageResourceLeave
                    )
                } else {
                    if (checkResourcesLeave.get().id != update.id) {
                        return responseBadRequest(
                            message = messageResourceLeave
                        )
                    }
                }
            }

            val leaveRequestType: LeaveRequestType = validate["leaveRequestType"] as LeaveRequestType
            val countLeaveRequest: LeaveRequestCheckDateResponse =
                validate["countLeaveRequest"] as LeaveRequestCheckDateResponse
            val resourcesReplacementId: String = validate["resourcesReplacementId"] as String

            var data = LeaveRequest()
            var oldData: LeaveRequest? = null

            if (update != null) {
                data = update
                data.updatedAt = Date()

                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                oldData = objectMapper.readValue(
                    objectMapper.writeValueAsString(update), LeaveRequest::class.java
                ) as LeaveRequest
            }

            repo.save(data.apply {
                this.resourcesId = resources.id
                this.startDate = startDate
                this.daysLength = countLeaveRequest.totalLeaveDays
                this.worksDays = countLeaveRequest.worksDaysCount
                this.holidays = countLeaveRequest.holidaysCount
                this.endDate = endDate
                this.leaveRequestTypeId = request.leaveRequestTypeId
                this.leaveRequestType = leaveRequestType
                this.description = request.description
                this.address = request.address
            })

            if (update != null) {
                repoResourcesReplacement.getByTaskIdAndTaskTypeAndActive(data.id, TASK_TYPE_LEAVE_REQUEST)
                    .forEach { rr ->
                        rr.resourcesId = resourcesReplacementId
                        rr.resourcesReplaceId = resources.id
                        rr.startDate = data.startDate
                        rr.updatedAt = Date()
                        repoResourcesReplacement.save(rr)
                    }
            } else {
                repoResourcesReplacement.save(
                    ResourcesReplacement(
                        resourcesId = resourcesReplacementId,
                        resourcesReplaceId = resources.id,
                        taskId = data.id,
                        taskType = TASK_TYPE_LEAVE_REQUEST,
                        startDate = data.startDate,
                        endDate = data.endDate
                    )
                )
            }

            val dformNotif: DateFormat = SimpleDateFormat("dd MMMM yyyy")
            val taskDescription =
                "Mengajukan cuti ${leaveRequestType.name} mulai tanggal ${dformNotif.format(startDate)} sampai ${
                    dformNotif.format(endDate)
                }"

            var supervisiorUserId: String? = null
            if (!resources.supervisiorId.isNullOrEmpty()) {
                val supervisior = repoResources.findByIdAndActive(resources.supervisiorId)
                if (supervisior.isPresent) {
                    supervisiorUserId = supervisior.get().userId
                }
            }

            serviceTask.createOrUpdateFromTask(
                taskId = data.id ?: "",
                taskType = TASK_TYPE_LEAVE_REQUEST,
                users = getUserLogin(),
                description = taskDescription,
                receiver = supervisiorUserId,
                update = update != null
            )

            val scheDuleDesciption =
                "cuti ${leaveRequestType.name} mulai tanggal ${dformNotif.format(startDate)} sampai ${
                    dformNotif.format(endDate)
                }"

            val schedule = ResourcesSchedule(
                resourcesId = data.resourcesId,
                taskId = data.id,
                taskType = TASK_TYPE_LEAVE_REQUEST,
                approveStatus = data.approveStatus,
                startDate = data.startDate,
                endDate = data.endDate,
                daysLength = countLeaveRequest.worksDaysCount,
                description = scheDuleDesciption
            )

            serviceResourcesSchedule.createFromTask(schedule, update != null)

            if (supervisiorUserId == null) {
                approve(
                    ApproveRequest(
                        id = data.id,
                        approveStatus = APPROVAL_STATUS_APPROVE
                    )
                )
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "leave_request",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approve(request: ApproveRequest): ResponseEntity<ReturnData> {
        try {
            val resources = repoResources.findByUserIdAndActive(getUserLogin()?.id)
            if (!resources.isPresent) {
                return responseBadRequest(message = "User tidak terdaftar sebagai staff")
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            var oldData: LeaveRequest? = null
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(find.get()), LeaveRequest::class.java
            ) as LeaveRequest

            val validate = validateApprove(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            find.get().approveStatus = request.approveStatus
            find.get().rejectedMessage = request.rejectedMessage
            find.get().updatedBy = resources.get().id
            find.get().updatedAt = Date()
            repo.save(find.get())

            serviceTask.approveRejectFromTask(request.approveStatus ?: 0, find.get().id ?: "", TASK_TYPE_LEAVE_REQUEST)

            serviceResourcesSchedule.approveFromTask(
                request.approveStatus,
                find.get().resourcesId,
                find.get().id,
                TASK_TYPE_LEAVE_REQUEST
            )

            val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
            val userId = getUserLogin()?.id ?: ""
            Thread {
                logActivityService.writeChangeData(
                    find.get(),
                    oldData,
                    "leave_request",
                    ipAdress,
                    userId
                )
            }.start()

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                return responseSuccess(data = data)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun checkDate(request: LeaveRequestCheckDateRequest): ResponseEntity<ReturnData> {
        try {
            val leaveRequestType = repoLeaveRequestType.findByIdAndActive(request.leaveRequestTypeId)
            if (!leaveRequestType.isPresent) {
                return responseNotFound()
            }

            val validate = validateCheckDate(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val startDate: Date = validate["startDate"] as Date
            val endDate: Date = validate["endDate"] as Date

            return responseSuccess(data = countLeaveRequest(startDate, endDate, leaveRequestType.get()))
        } catch (e: Exception) {
            throw e
        }
    }

    private fun countLeaveRequest(
        startDate: Date,
        endDate: Date,
        leaveRequestType: LeaveRequestType,
    ): LeaveRequestCheckDateResponse {
        try {
            var worksDaysCount = 0
            var holidaysCount = 0

            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val days = (getDaysBetween(startDate, endDate, sdf) + 1)
            val holidays = repoHolidayManagement.getByActive()

            var startDateCheck: LocalDate = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
            for (i in 1..days) {
                if (LEAVE_REQUEST_DEFAULT_HOLIDAYS_DAYS.contains(startDateCheck.dayOfWeek.value)) {
                    holidaysCount++
                } else {
                    worksDaysCount++
                }

                val findHoliday = holidays.filter {
                    sdf.parse(sdf.format(it.holidayDate)) == Date.from(
                        startDateCheck.atStartOfDay(ZoneId.systemDefault()).toInstant()
                    )
                }

                findHoliday.forEach { ff ->
                    val holidayLocale: LocalDate? =
                        ff.holidayDate?.toInstant()?.atZone(ZoneId.systemDefault())?.toLocalDate()
                    if (holidayLocale != null) {
                        if (!LEAVE_REQUEST_DEFAULT_HOLIDAYS_DAYS.contains(holidayLocale.dayOfWeek.value)) {
                            holidaysCount++
                            worksDaysCount--
                        }
                    }
                }

                startDateCheck = startDateCheck.plusDays(1)
            }

            var totalLeaveDays: Int = worksDaysCount
            if (leaveRequestType.workDays == false) {
                totalLeaveDays += holidaysCount
            }

            return LeaveRequestCheckDateResponse(worksDaysCount, holidaysCount, totalLeaveDays)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(
        request: LeaveRequestRequest,
        resources: UsersResources,
    ): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            var dateStart: Date? = null
            var dateEnd: Date? = null
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                dateStart = dform.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate",
                        "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            try {
                dateEnd = dform.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate",
                        "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateEnd?.before(dateStart) == true) {
                listMessage.add(
                    ErrorMessage(
                        "endDate",
                        "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            val now = dform.parse(dform.format(Date()))

            if (dateStart?.before(now) == true) {
                listMessage.add(
                    ErrorMessage(
                        "startDate",
                        "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            rData["startDate"] = dateStart as Date
            rData["endDate"] = dateEnd as Date

            val checkLeaveType = repoLeaveRequestType.findByIdAndActive(request.leaveRequestTypeId)
            if (!checkLeaveType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "endDate",
                        "Jenis Izin id ${request.leaveRequestTypeId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                val countLeaveRequest = countLeaveRequest(dateStart, dateEnd, checkLeaveType.get())

                if (checkLeaveType.get().maxDayLength != null) {
                    if (countLeaveRequest.totalLeaveDays > checkLeaveType.get().maxDayLength!!) {
                        listMessage.add(
                            ErrorMessage(
                                "leaveRequestTypeId",
                                "Jenis Izin ${checkLeaveType.get().name} hanya ${checkLeaveType.get().maxDayLength} hari"
                            )
                        )
                    }
                }

                rData["countLeaveRequest"] = countLeaveRequest
                rData["leaveRequestType"] = checkLeaveType.get()
            }


            if (request.resourcesReplacement.isNullOrEmpty()) { // ganti dengan supervisiornya
                if (!resources.supervisiorId.isNullOrEmpty()) {
                    val supervisior = repoResources.findByIdAndActive(resources.supervisiorId)
                    if (supervisior.isPresent) {
                        request.resourcesReplacement = supervisior.get().id
                    }
                }
            }

            if (!request.resourcesReplacement.isNullOrEmpty()) {
                val checkReplaceResource = repoResources.findByIdAndActive(request.resourcesReplacement)
                if (!checkReplaceResource.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "resourcesReplacement",
                            "Resouces id ${request.resourcesReplacement} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["resourcesReplacementId"] = checkReplaceResource.get().id ?: ""

                    val checkLeaveRequest =
                        repo.findByResourcesIdStartDateAndDate(
                            request.resourcesReplacement, dateStart, dateEnd, listOf(
                                APPROVAL_STATUS_REQUEST, APPROVAL_STATUS_APPROVE
                            )
                        )
                    if (checkLeaveRequest.isPresent) { // pengganti sedang cuti
                        // cari di resource penggantinya
                        val findReplaceResource =
                            repoResourcesReplacement.getByResourcesReplaceIdDateTaskIdTaskTypeApproved(
                                checkLeaveRequest.get().resourcesId,
                                dateStart,
                                dateEnd,
                                checkLeaveRequest.get().id,
                                TASK_TYPE_LEAVE_REQUEST
                            )
                        if (findReplaceResource.isNotEmpty()) {
                            rData["resourcesReplacementId"] = findReplaceResource[0].resourcesId ?: ""
                        } else {
                            listMessage.add(
                                ErrorMessage(
                                    "resourcesReplacement",
                                    "Pelaksana tugas selama izin $VALIDATOR_MSG_NOT_FOUND"
                                )
                            )
                        }
                    }
                }
            } else {
                listMessage.add(
                    ErrorMessage(
                        "resourcesReplacement",
                        "Supervisi Pelaksana Tugas selama izin $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateApprove(request: ApproveRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                if (request.rejectedMessage.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "rejectedMessage",
                            "Alasan Penolakan $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateCheckDate(
        request: LeaveRequestCheckDateRequest
    ): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            var dateStart: Date? = null
            var dateEnd: Date? = null
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                dateStart = dform.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate",
                        "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            try {
                dateEnd = dform.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate",
                        "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            val now = dform.parse(dform.format(Date()))

            if (dateStart?.before(now) == true) {
                listMessage.add(
                    ErrorMessage(
                        "startDate",
                        "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            rData["startDate"] = dateStart as Date
            rData["endDate"] = dateEnd as Date
            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }
}
