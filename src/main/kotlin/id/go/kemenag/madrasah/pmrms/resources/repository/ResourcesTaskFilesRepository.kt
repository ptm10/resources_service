package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesTaskFiles
import org.springframework.data.jpa.repository.JpaRepository

interface ResourcesTaskFilesRepository : JpaRepository<ResourcesTaskFiles, String> {
}
