package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Position
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface PositionRepository : JpaRepository<Position, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Position>

    @Query("FROM Position WHERE LOWER(TRIM(:name)) = LOWER(TRIM(name)) AND unitId = :unitId AND active = :active")
    fun findByNameUnitIdActive(name: String?, unitId: String?, active: Boolean? = true): Optional<Position>
}
