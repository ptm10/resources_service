package id.go.kemenag.madrasah.pmrms.resources.model.request

import id.go.kemenag.madrasah.pmrms.resources.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ApproveRequest(

    @field:NotEmpty(message = "id $VALIDATOR_MSG_REQUIRED")
    var id: String? = null,

    @field:NotNull(message = "Status $VALIDATOR_MSG_REQUIRED")
    var approveStatus: Int? = null,

    var rejectedMessage: String? = null
)
