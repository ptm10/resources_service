package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.DeleteDataRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.ResourcesTaskCommentRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.ResourcesTaskRequest
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.ResourcesTaskService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Resources Task"], description = "Resources Task API")
@RestController
@RequestMapping(path = ["resources-task"])
class ResourcesTaskController {

    @Autowired
    private lateinit var service: ResourcesTaskService

    @PostMapping(value = ["datatable-supervisior"], produces = ["application/json"])
    fun datatableSupervisior(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableSupervisior(req)
    }

    @PostMapping(value = ["datatable-staff"], produces = ["application/json"])
    fun datatableStaff(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableStaff(req)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun save(@Valid @RequestBody request: ResourcesTaskRequest): ResponseEntity<ReturnData> {
        return service.save(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable id: String,
        @Valid @RequestBody request: ResourcesTaskRequest
    ): ResponseEntity<ReturnData> {
        return service.update(id, request)
    }

    @DeleteMapping(value = [""], produces = ["application/json"])
    fun delete(@Valid @RequestBody request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return service.delete(request)
    }

    @PostMapping(value = ["create-comment"], produces = ["application/json"])
    fun createComment(@Valid @RequestBody request: ResourcesTaskCommentRequest): ResponseEntity<ReturnData> {
        return service.createComment(request)
    }

    @DeleteMapping(value = ["delete-comment"], produces = ["application/json"])
    fun deleteComment(@Valid @RequestBody request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return service.deleteComment(request)
    }
}
