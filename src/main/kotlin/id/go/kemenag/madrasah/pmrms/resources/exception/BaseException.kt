package id.go.kemenag.madrasah.pmrms.resources.exception

import org.springframework.http.HttpStatus

open class BaseException : RuntimeException() {

    var code = "500"
    override var message = ""
    var status = HttpStatus.INTERNAL_SERVER_ERROR
    var data: Any? = null

}
