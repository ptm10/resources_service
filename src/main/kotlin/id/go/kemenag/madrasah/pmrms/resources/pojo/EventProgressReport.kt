package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_progress_report", schema = "public")
data class EventProgressReport(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = null,

    @Column(name = "event_output")
    var eventOutput: String? = null,

    @Column(name = "participant_male")
    var participantMale: Long? = null,

    @Column(name = "participant_female")
    var participantFemale: Long? = null,

    @Column(name = "participant_count")
    var participantCount: Long? = null,

    @Column(name = "participant_other_information")
    var participantOtherInformation: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "province_id")
    var provinceId: String? = null,

    @Column(name = "regency_id")
    var regencyId: String? = null,

    @Column(name = "location")
    var location: String? = null,

    @Column(name = "attachment_file_id")
    var attachmentFileId: String? = null,

    @ManyToOne
    @JoinColumn(name = "attachment_file_id", insertable = false, updatable = false, nullable = true)
    var attachmentFile: Files? = null,

    @Column(name = "budget_available")
    var budgetAvailable: Boolean? = null,

    @Column(name = "pok_event")
    var pokEvent: Boolean? = null,

    @Column(name = "rab_file_id")
    var rabFileId: String? = null,

    @ManyToOne
    @JoinColumn(name = "rab_file_id", insertable = false, updatable = false, nullable = true)
    var rabFile: Files? = null,

    @Column(name = "status")
    var status: Int? = null,

    @Column(name = "created_by")
    var createdBy: String? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
