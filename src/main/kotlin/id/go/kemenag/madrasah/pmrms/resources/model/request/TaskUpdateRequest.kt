package id.go.kemenag.madrasah.pmrms.resources.model.request

import javax.validation.constraints.NotNull

data class TaskUpdateRequest(

    @field:NotNull
    var status: Int? = null
)
