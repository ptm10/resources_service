package id.go.kemenag.madrasah.pmrms.resources.model.request

import id.go.kemenag.madrasah.pmrms.resources.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class LeaveRequestCheckDateRequest(

    @field:NotEmpty(message = "Leave Request Type Id $VALIDATOR_MSG_REQUIRED")
    var leaveRequestTypeId: String? = null,

    @field:NotEmpty(message = "Tanggal Mulai $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "Tanggal Selesai $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null
)

