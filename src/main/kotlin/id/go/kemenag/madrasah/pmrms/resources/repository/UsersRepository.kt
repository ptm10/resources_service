package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Users
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface UsersRepository : JpaRepository<Users, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Users>

    @Query("select ur.id from Users ur where ur.componentId = :componentId and ur.active = :active")
    fun getIdsByComponentid(
        @Param("componentId") componentId: String?,
        @Param("active") active: Boolean? = true
    ): List<String>

    @Query("select ur.id from Users ur where ur.provinceId = :provinceId and ur.active = :active")
    fun getIdsByProvinceIdAndActive(provinceId: String?, active: Boolean? = true): List<String>
}
