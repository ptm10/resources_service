package id.go.kemenag.madrasah.pmrms.resources.exception

import org.springframework.http.HttpStatus

/**
 * The server understands the content type of the request entity,
 * and the syntax of the request entity is correct
 * but was unable to process the contained instructions.
 */
class UnProcessableEntityException : BaseException {

    constructor() {
        this.code = "422"
        this.message = "UnProcessable Entity"
        this.status = HttpStatus.UNPROCESSABLE_ENTITY
    }

    constructor(message: String?) {
        this.code = "422"
        this.message = message!!
        this.status = HttpStatus.UNPROCESSABLE_ENTITY
    }

    constructor(message: String? = null, data: Any? = null) {
        this.code = "422"
        this.message = message ?: "UnProcessable Entity"
        this.status = HttpStatus.UNPROCESSABLE_ENTITY
        this.data = data
    }

}
