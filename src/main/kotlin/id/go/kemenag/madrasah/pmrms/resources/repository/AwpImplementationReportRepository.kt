package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.AwpImplementationReport
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AwpImplementationReportRepository : JpaRepository<AwpImplementationReport, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<AwpImplementationReport>
}
