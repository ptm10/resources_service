package id.go.kemenag.madrasah.pmrms.resources.constant

const val TOKEN_PREFIX = "Bearer "
const val HEADER_STRING = "Authorization"

val USER_ADMIN_ALLOWED_PATH =
    listOf(
        "/resources/*",
        "/resources-schedule/*",
        "/task/*",
        "/task-notification/*",
        "/leave-request/*",
        "/resources-replacement/*",
        "/transportation/*",
        "/event/*",
        "/out-of-town-event/*",
        "/pdo/*",
        "/iri/*",
        "/task-resources-tag/*",
        "/task-resources/*",
        "/resources-task/*"
    )

val AUDIENCE_FILTER_PATH = mapOf(
    "user-admin" to USER_ADMIN_ALLOWED_PATH
)

val EMAIL_VALID_REGEX = "@madrasah.kemenag.go.id"
