package id.go.kemenag.madrasah.pmrms.resources.repository

import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface UnitRepository : JpaRepository<id.go.kemenag.madrasah.pmrms.resources.pojo.Unit, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<id.go.kemenag.madrasah.pmrms.resources.pojo.Unit>
}
