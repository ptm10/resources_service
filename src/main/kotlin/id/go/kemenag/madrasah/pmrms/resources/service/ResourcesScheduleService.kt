package id.go.kemenag.madrasah.pmrms.resources.service

import id.go.kemenag.madrasah.pmrms.resources.constant.LEAVE_REQUEST_MAX_DAY_IN_YEAR
import id.go.kemenag.madrasah.pmrms.resources.constant.TASK_TITTLES
import id.go.kemenag.madrasah.pmrms.resources.constant.TASK_TYPE_LEAVE_REQUEST
import id.go.kemenag.madrasah.pmrms.resources.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ResourcesScheduleResponse
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.pojo.Resources
import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesSchedule
import id.go.kemenag.madrasah.pmrms.resources.repository.ResourcesRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.ResourcesScheduleRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.ResourcesScheduleRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class ResourcesScheduleService {

    @Autowired
    private lateinit var repo: ResourcesScheduleRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoNative: ResourcesScheduleRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            var resources: Resources? = null
            var respurcesId: String? = null

            if ((req.paramIs?.size ?: 0) > 0) {
                respurcesId = req.paramIs?.get(0)?.value ?: ""
            }

            val check = repoResources.findByIdAndActive(respurcesId)
            if (check.isPresent) {
                resources = check.get()
            }

            var leaveDaysRemaining = LEAVE_REQUEST_MAX_DAY_IN_YEAR
            val leaveDaysCount =
                repo.sumDaysLengthByResourcesTaskTypeApproveStatusActive(resources?.id, TASK_TYPE_LEAVE_REQUEST) ?: 0
            if (leaveDaysCount > leaveDaysRemaining) {
                leaveDaysRemaining = 0
            } else {
                leaveDaysRemaining -= leaveDaysCount
            }

            val data = ResourcesScheduleResponse(
                resources = resources,
                leaveDaysCount = leaveDaysCount,
                leaveDaysRemaining = leaveDaysRemaining
            )

            val datas: MutableList<ResourcesSchedule>? = repoNative.getPage(req)?.content
            if (datas != null) {
                data.content = datas
            }

            responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun createFromTask(
        req: ResourcesSchedule,
        update: Boolean = false
    ) {
        try {
            var schedule: ResourcesSchedule? = ResourcesSchedule()
            if (update) {
                val findSchedule =
                    repo.findByResourcesIdAndTaskIdAndTaskTypeAndActive(req.resourcesId, req.taskId, req.taskType)
                if (findSchedule.isPresent) {
                    schedule = findSchedule.get()
                    schedule.updatedAt = Date()
                }
            }

            if (schedule != null) {
                repo.save(schedule.apply {
                    this.resourcesId = req.resourcesId
                    this.taskType = req.taskType
                    this.taskId = req.taskId
                    this.approveStatus = req.approveStatus
                    this.startDate = req.startDate
                    this.endDate = req.endDate
                    this.daysLength = req.daysLength
                    this.title = TASK_TITTLES[taskType]
                    this.description = req.description
                })
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveFromTask(approveStatus: Int?, resourcesId: String?, taskId: String?, taskType: Int?) {
        try {
            val findSchedule =
                repo.findByResourcesIdAndTaskIdAndTaskTypeAndActive(resourcesId, taskId, taskType)
            if (findSchedule.isPresent) {
                findSchedule.get().approveStatus = approveStatus
                findSchedule.get().updatedAt = Date()
                repo.save(findSchedule.get())
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun deleteFromTask(resourcesId: String?, taskId: String?, taskType: Int?) {
        try {
            val findSchedule =
                repo.findByResourcesIdAndTaskIdAndTaskTypeAndActive(resourcesId, taskId, taskType)
            if (findSchedule.isPresent) {
                findSchedule.get().active = false
                findSchedule.get().updatedAt = Date()
                repo.save(findSchedule.get())
            }
        } catch (e: Exception) {
            throw e
        }
    }

}
