package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.EventOutOfTown
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface EventOutOfTownRepository : JpaRepository<EventOutOfTown, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<EventOutOfTown>
}
