package id.go.kemenag.madrasah.pmrms.resources.model.response

import id.go.kemenag.madrasah.pmrms.resources.pojo.Files

data class ReturnDataFiles(
    var success: Boolean? = false,
    var data: Files? = null,
    var message: String? = null
)
