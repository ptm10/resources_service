package id.go.kemenag.madrasah.pmrms.resources.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.helpers.getUserLogin
import id.go.kemenag.madrasah.pmrms.resources.helpers.responseNotFound
import id.go.kemenag.madrasah.pmrms.resources.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.resources.model.TaskNotificationUpdateByTaskRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.resources.model.request.TaskNotificationUpdateRequest
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskNotification
import id.go.kemenag.madrasah.pmrms.resources.repository.ResourcesReplacementRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.ResourcesRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.TaskNotificationRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.TaskNotificationRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Service
class TaskNotificationService {

    @Autowired
    private lateinit var repo: TaskNotificationRepository

    @Autowired
    private lateinit var repoNative: TaskNotificationRepositoryNative

    @Autowired
    private lateinit var repoResourcesReplacement: ResourcesReplacementRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var logActivityService: LogActivityService

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val userIds = mutableListOf<String>()
            userIds.add(getUserLogin()?.id ?: "")

            val resource = repoResources.findByUserIdAndActive(getUserLogin()?.id)
            if (resource.isPresent) {
                // add task from replacement
                val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                repoResourcesReplacement.findByResourcesIdDateApproved(
                    resource.get().id, dform.parse(dform.format(Date()))
                ).forEach {
                    if (!userIds.contains(it.resourcesReplace?.userId)) {
                        userIds.add(it.resourcesReplace?.userId ?: "")
                    }
                }
            }

            req.paramIn?.add(ParamArray("userId", dataType = "string", userIds))

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: TaskNotificationUpdateRequest): ResponseEntity<ReturnData> {
        try {
            val find = repo.findByIdAndActive(id)
            if (find.isPresent) {
                var oldData: TaskNotification? = null
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                oldData = objectMapper.readValue(
                    objectMapper.writeValueAsString(find.get()), TaskNotification::class.java
                ) as TaskNotification

                find.get().readed = request.readed
                repo.save(find.get())

                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        find.get(),
                        oldData,
                        "task_notification",
                        ipAdress,
                        userId
                    )
                }.start()

                return responseSuccess(data = find.get())
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateByTaskIdTaskType(request: TaskNotificationUpdateByTaskRequest): ResponseEntity<*>? {
        try {
            repo.getByTaskIdAndTaskTypeAndUserIdAndActive(request.taskId, request.taskType, getUserLogin()?.id)
                .forEach {
                    it.readed = true
                    it.updatedAt = Date()
                    repo.save(it)
                }
            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }
}
