package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Pdo
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface PdoRepository : JpaRepository<Pdo, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Pdo>
}
