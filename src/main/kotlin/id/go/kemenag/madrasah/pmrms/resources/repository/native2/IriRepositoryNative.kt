package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.Iri
import org.springframework.stereotype.Repository

@Repository
class IriRepositoryNative : BaseRepositoryNative<Iri>(Iri::class.java)
