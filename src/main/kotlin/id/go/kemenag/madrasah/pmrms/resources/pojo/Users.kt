package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users", schema = "auth")
data class Users(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "email")
    var email: String? = null,

    @Column(name = "password")
    @JsonIgnore
    var password: String? = null,

    @Column(name = "first_name")
    var firstName: String? = null,

    @Column(name = "last_name")
    var lastName: String? = null,

    @Column(name = "profile_picture_id")
    var profilePictureId: String? = null,

    @ManyToOne
    @JoinColumn(name = "profile_picture_id", insertable = false, updatable = false, nullable = true)
    var profilePicture: Files? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "component_id", insertable = false, updatable = false, nullable = true)
    var component: Component? = null,

    @Column(name = "province_id")
    var provinceId: String? = null,

    @ManyToOne
    @JoinColumn(name = "province_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var province: Province? = null,

    @OneToMany(mappedBy = "userId")
    @Where(clause = "active = true")
    var roles: MutableSet<UsersRoleResources>? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
