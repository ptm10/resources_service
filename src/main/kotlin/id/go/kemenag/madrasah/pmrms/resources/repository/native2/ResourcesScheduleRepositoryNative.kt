package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesSchedule
import org.springframework.stereotype.Repository

@Repository
class ResourcesScheduleRepositoryNative : BaseRepositoryNative<ResourcesSchedule>(ResourcesSchedule::class.java)
