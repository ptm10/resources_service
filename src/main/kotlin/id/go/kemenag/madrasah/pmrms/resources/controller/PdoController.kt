package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.PdoService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Api(tags = ["Pdo"], description = "Pdo API")
@RestController
@RequestMapping(path = ["pdo"])
class PdoController {

    @Autowired
    private lateinit var service: PdoService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @GetMapping(value = ["with-iri"], produces = ["application/json"])
    fun withIri(): ResponseEntity<ReturnData> {
        return service.withIri()
    }

    @GetMapping(value = ["get-by-component-id"], produces = ["application/json"])
    fun getByComponentId(@RequestParam("id") id: String?): ResponseEntity<ReturnData> {
        return service.getByComponentId(id)
    }
}
