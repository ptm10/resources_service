package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "files", schema = "public")
data class Files(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "filepath")
    var filepath: String? = null,

    @Column(name = "filename")
    var filename: String? = null,

    @Column(name = "file_ext")
    var fileExt: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date()
)
