package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.LogActivity
import org.springframework.data.jpa.repository.JpaRepository

interface LogActivityRepository : JpaRepository<LogActivity, String> {

}
