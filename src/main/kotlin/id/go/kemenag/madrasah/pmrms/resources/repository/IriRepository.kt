package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Iri
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface IriRepository : JpaRepository<Iri, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Iri>

    fun getByCodeAndActiveOrderByName(code: String?, active: Boolean? = true): List<Iri>
}
