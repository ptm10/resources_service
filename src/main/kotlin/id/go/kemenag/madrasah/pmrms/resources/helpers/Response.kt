package id.go.kemenag.madrasah.pmrms.resources.helpers

import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

fun responseSuccess(message: String = "Success", data: Any? = null): ResponseEntity<ReturnData> {
    return ResponseEntity<ReturnData>(
        ReturnData(
            success = true,
            message = message,
            data = data
        ),
        HttpStatus.OK
    )
}

fun responseCreated(message: String = "Created", data: Any? = null): ResponseEntity<ReturnData> {
    return ResponseEntity<ReturnData>(
        ReturnData(
            success = true,
            message = message,
            data = data
        ),
        HttpStatus.CREATED
    )
}

fun responseInternalServerError(
    message: String = "Terjadi Kesalahan Sistem. Silahkan Ulangi beberapa saat lagi",
    data: Any? = null
): ResponseEntity<ReturnData> {
    return ResponseEntity<ReturnData>(
        ReturnData(
            success = false,
            message = message,
            data = data
        ),
        HttpStatus.INTERNAL_SERVER_ERROR
    )
}

fun responseNotFound(message: String = "Data tidak di temukan", data: Any? = null): ResponseEntity<ReturnData> {
    return ResponseEntity<ReturnData>(
        ReturnData(
            success = false,
            message = message,
            data = data
        ),
        HttpStatus.NOT_FOUND
    )
}

fun responseUnprocessableEntity(
    message: String = "Unprocessable Entity",
    data: Any? = null
): ResponseEntity<ReturnData> {
    return ResponseEntity<ReturnData>(
        ReturnData(
            success = false,
            message = message,
            data = data
        ),
        HttpStatus.UNPROCESSABLE_ENTITY
    )
}

fun responseBadRequest(message: String = "Bad Request", data: Any? = null): ResponseEntity<ReturnData> {
    return ResponseEntity<ReturnData>(
        ReturnData(
            success = false,
            message = message,
            data = data
        ),
        HttpStatus.BAD_REQUEST
    )
}
