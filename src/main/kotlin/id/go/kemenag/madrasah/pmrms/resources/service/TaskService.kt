package id.go.kemenag.madrasah.pmrms.resources.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.constant.*
import id.go.kemenag.madrasah.pmrms.resources.helpers.*
import id.go.kemenag.madrasah.pmrms.resources.model.TaskUpdateByTypeRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.resources.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.resources.model.request.TaskUpdateRequest
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.model.users.Users
import id.go.kemenag.madrasah.pmrms.resources.model.users.UsersResources
import id.go.kemenag.madrasah.pmrms.resources.pojo.Task
import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskNotification
import id.go.kemenag.madrasah.pmrms.resources.repository.*
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.TaskRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Service
class TaskService {

    @Autowired
    private lateinit var repo: TaskRepository

    @Autowired
    private lateinit var repoTaskNotification: TaskNotificationRepository

    @Autowired
    private lateinit var repoNative: TaskRepositoryNative

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoResourcesReplacement: ResourcesReplacementRepository

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var repoAwpImplementationReport: AwpImplementationReportRepository

    @Autowired
    private lateinit var repoAwpImplementation: AwpImplementationRepository

    @Autowired
    private lateinit var repoEventProgressReport: EventProgressReportRepository

    @Autowired
    private lateinit var repoEventReport: EventReportRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var logActivityService: LogActivityService

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                req.paramIn?.add(ParamArray("createdFor", "string", listOf(getUserLogin()?.id ?: "")))

                val taskUserIds: MutableList<String> = mutableListOf()

                val resource: UsersResources? = getUserLogin()?.resources
                if (resource != null) {
                    val listStaff = repoResources.findBySupervisiorIdAndActive(resource.id)
                    listStaff.forEach {
                        if (it.userId != getUserLogin()?.id) {
                            taskUserIds.add(it.userId ?: "")
                        }
                    }

                    // add task from replacement
                    val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                    repoResourcesReplacement.findByResourcesIdDateApproved(
                        resource.id, dform.parse(dform.format(Date()))
                    ).forEach {
                        repoResources.findBySupervisiorIdAndActive(it.resourcesReplaceId).forEach { ls ->
                            if (ls.userId != getUserLogin()?.id) {
                                if (!taskUserIds.contains(ls.userId)) {
                                    taskUserIds.add(ls.userId ?: "")
                                }
                            }
                        }
                    }
                }

                if (taskUserIds.isNotEmpty()) {
                    req.paramIn?.add(ParamArray("createdBy", "string", taskUserIds))
                }
            } else {
                req.paramNotIn?.add(ParamArray("createdBy", "string", listOf(getUserLogin()?.id ?: "")))
            }

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableCreator(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            req.paramIs?.add(ParamSearch("createdBy", "string", getUserLogin()?.id))
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateByTaskIdTaskType(req: TaskUpdateByTypeRequest): ResponseEntity<ReturnData> {
        try {
            var find: Task? = null
            if (req.taskType != TASK_TYPE_AWP_IMPLEMENTATION && req.taskType != TASK_TYPE_EVENT && req.taskType != TASK_TYPE_EVENT_PROGRESS_REPORT && req.taskType != TASK_TYPE_EVENT_REPORT && req.taskType != TASK_TYPE_AWP_IMPLEMENTATION_REPORT) {
                val check = repo.findByTaskIdAndTaskTypeAndActive(req.taskId, req.taskType)
                if (check.isPresent) {
                    find = check.get()
                }
            } else {
                if (req.taskType == TASK_TYPE_EVENT) {
                    var taskNumber = TASK_NUMBER_TASK
                    val event = repoEvent.findByIdAndActive(req.taskId)
                    if (event.isPresent) {
                        if (event.get().status == EVENT_STATUS_PLANNING_REVISION || event.get().status == EVENT_STATUS_EVELUATION_COORDINATOR_REVISION || event.get().status == EVENT_STATUS_EVALUATION_TREASURER_REVISION) {
                            taskNumber = TASK_NUMBER_TASK_REVISION
                        }
                    }

                    val check = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                        req.taskId, req.taskType, getUserLogin()?.id, taskNumber
                    )

                    if (check.isPresent) {
                        find = check.get()
                    }
                } else if (req.taskType == TASK_TYPE_AWP_IMPLEMENTATION) {
                    var taskNumber = TASK_NUMBER_TASK
                    val awpImplementation = repoAwpImplementation.findByIdAndActive(req.taskId)
                    if (awpImplementation.isPresent) {
                        if (awpImplementation.get().status == AWP_IMPLEMENTATION_STATUS_PLANNING_REVISION || awpImplementation.get().status == AWP_IMPLEMENTATION_STATUS_EVELUATION_REVISION) {
                            taskNumber = TASK_NUMBER_TASK_REVISION
                        }
                    }

                    val check = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                        req.taskId, req.taskType, getUserLogin()?.id, taskNumber
                    )

                    if (check.isPresent) {
                        find = check.get()
                    }
                } else if (req.taskType == TASK_TYPE_EVENT_PROGRESS_REPORT) {
                    var taskNumber = TASK_NUMBER_TASK
                    val eventProgressReport = repoEventProgressReport.findByIdAndActive(req.taskId)
                    if (eventProgressReport.isPresent) {
                        if (eventProgressReport.get().status == EVENT_PROGRESS_REPORT_STATUS_REVISION) {
                            taskNumber = TASK_NUMBER_TASK_REVISION
                        }
                    }

                    val check = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                        req.taskId, req.taskType, getUserLogin()?.id, taskNumber
                    )

                    if (check.isPresent) {
                        find = check.get()
                    }
                } else if (req.taskType == TASK_TYPE_EVENT_REPORT) {
                    if (userHasAllRoles(
                            listOf(
                                ROLE_ID_ADMINSTRATOR, ROLE_ID_COORDINATOR
                            )
                        ) && getUserLogin()?.componentId == COMPONENT_ID_PMU
                    ) {
                        val taskNotification =
                            repoTaskNotification.getByTaskIdAndTaskTypeAndActive(req.taskId, req.taskType)

                        taskNotification.forEach { tn ->
                            tn.active = false
                            tn.updatedAt = Date()
                            repoTaskNotification.save(tn)
                        }
                    } else {
                        var taskNumber = TASK_NUMBER_TASK
                        val eventReport = repoEventReport.findByIdAndActive(req.taskId)
                        if (eventReport.isPresent) {
                            if (eventReport.get().status == EVENT_REPORT_STATUS_REVISION) {
                                taskNumber = TASK_NUMBER_TASK_REVISION
                            }
                        }

                        val check = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                            req.taskId, req.taskType, getUserLogin()?.id, taskNumber
                        )

                        if (check.isPresent) {
                            find = check.get()
                        }
                    }
                } else if (req.taskType == TASK_TYPE_AWP_IMPLEMENTATION_REPORT) {
                    if (userHasAllRoles(
                            listOf(
                                ROLE_ID_ADMINSTRATOR, ROLE_ID_COORDINATOR
                            )
                        ) && getUserLogin()?.componentId == COMPONENT_ID_PMU
                    ) {
                        val taskNotification =
                            repoTaskNotification.getByTaskIdAndTaskTypeAndActive(req.taskId, req.taskType)

                        taskNotification.forEach { tn ->
                            tn.active = false
                            tn.updatedAt = Date()
                            repoTaskNotification.save(tn)
                        }
                    } else {
                        var taskNumber = TASK_NUMBER_TASK
                        val awpImplementationReport = repoAwpImplementationReport.findByIdAndActive(req.taskId)
                        if (awpImplementationReport.isPresent) {
                            if (awpImplementationReport.get().status == AWP_IMPLEMENTATION_REPORT_STATUS_REVISION) {
                                taskNumber = TASK_NUMBER_TASK_REVISION
                            }
                        }

                        val check = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                            req.taskId, req.taskType, getUserLogin()?.id, taskNumber
                        )

                        if (check.isPresent) {
                            find = check.get()
                        }
                    }
                } else {
                    val check =
                        repo.findByTaskIdAndTaskTypeAndCreatedForAndActive(req.taskId, req.taskType, getUserLogin()?.id)
                    if (check.isPresent) {
                        find = check.get()
                    }
                }
            }

            if (find != null) {
                var allowUpdateTask = false
                if (req.taskType == TASK_TYPE_AWP_IMPLEMENTATION || req.taskType == TASK_TYPE_EVENT) {
                    if (find.status == TASK_STATUS_NEW) {
                        allowUpdateTask = true
                    }
                } else {
                    allowUpdateTask = true
                }

                if (allowUpdateTask) {
                    find.status = req.status
                    find.updatedAt = Date()
                    repo.save(find)
                }

                var taskNotification = repoTaskNotification.getByTaskIdAndTaskTypeAndActive(find.taskId, find.taskType)
                if (find.taskType == TASK_TYPE_AWP_IMPLEMENTATION || find.taskType == TASK_TYPE_EVENT) {
                    taskNotification = repoTaskNotification.getByTaskIdAndTaskTypeAndUserIdAndActive(
                        find.taskId, find.taskType, getUserLogin()?.id
                    )
                }

                taskNotification.forEach { tn ->
                    tn.readed = true

                    if (find.status == TASK_STATUS_NEW) {
                        tn.readed = false
                    }

                    tn.updatedAt = Date()
                    repoTaskNotification.save(tn)
                }

                return responseSuccess(data = find)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun update(id: String, request: TaskUpdateRequest): ResponseEntity<ReturnData> {
        try {
            val find = repo.findByIdAndActive(id)
            if (find.isPresent) {
                var oldData: Task? = null
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                oldData = objectMapper.readValue(
                    objectMapper.writeValueAsString(find.get()), Task::class.java
                ) as Task

                find.get().status = request.status
                repo.save(find.get())

                var taskNotification =
                    repoTaskNotification.getByTaskIdAndTaskTypeAndActive(find.get().taskId, find.get().taskType)

                if (find.get().taskType == TASK_TYPE_AWP_IMPLEMENTATION || find.get().taskType == TASK_TYPE_AWP_IMPLEMENTATION_REPORT) {
                    taskNotification = repoTaskNotification.getByTaskIdAndTaskTypeAndUserIdAndActive(
                        find.get().taskId, find.get().taskType, getUserLogin()?.id
                    )
                }

                taskNotification.forEach { tn ->
                    tn.readed = true

                    if (find.get().status == TASK_STATUS_NEW) {
                        tn.readed = false
                    }

                    tn.updatedAt = Date()
                    repoTaskNotification.save(tn)
                }

                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        find.get(), oldData, "task", ipAdress, userId
                    )
                }.start()

                return responseSuccess(data = find.get())
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun createOrUpdateFromTask(
        taskId: String,
        taskType: Int,
        users: Users?,
        description: String,
        receiver: String?,
        update: Boolean = false,
        createdFor: String? = null
    ) {
        try {
            var task: Task? = Task()
            if (update) {
                val findTask = repo.findByTaskIdAndTaskTypeAndActive(taskId, taskType)
                if (findTask.isPresent) {
                    task = findTask.get()
                    task.updatedAt = Date()
                }
            }

            if (task != null) {
                repo.save(task.apply {
                    this.createdBy = users?.id
                    this.createdFor = createdFor
                    this.taskType = taskType
                    this.taskId = taskId
                    this.description = description
                })
            }

            if (receiver != null) {
                if (update) {
                    repoTaskNotification.getByTaskIdAndTaskTypeAndActive(taskId, taskType).forEach { tn ->
                        tn.userId = receiver
                        tn.message = description
                        tn.updatedAt = Date()
                        repoTaskNotification.save(tn)
                    }
                } else {
                    repoTaskNotification.save(
                        TaskNotification(
                            userId = receiver,
                            taskType = taskType,
                            taskId = taskId,
                            message = description,
                            createdBy = users?.id
                        )
                    )
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveRejectFromTask(approveStatus: Int, taskId: String, taskType: Int) {
        try {
            if (approveStatus == APPROVAL_STATUS_APPROVE || approveStatus == APPROVAL_STATUS_REJECT) {
                var taskStatus = TASK_STATUS_APPROVED
                if (approveStatus == APPROVAL_STATUS_REJECT) {
                    taskStatus = TASK_STATUS_REJECT
                }

                val task = repo.findByTaskIdAndTaskTypeAndActive(taskId, taskType)
                if (task.isPresent) {
                    task.get().updatedBy = getUserLogin()?.id
                    task.get().done = true
                    task.get().updatedAt = Date()
                    task.get().status = taskStatus
                    repo.save(task.get())
                }

                repoTaskNotification.getByTaskIdAndTaskTypeAndActive(taskId, taskType).forEach { tn ->
                    tn.updatedAt = Date()
                    tn.active = false
                    repoTaskNotification.save(tn)
                }

                if (approveStatus == APPROVAL_STATUS_APPROVE && taskType == TASK_TYPE_LEAVE_REQUEST) { // approve replacement
                    repoResourcesReplacement.getByTaskIdAndTaskTypeAndActive(taskId, TASK_TYPE_LEAVE_REQUEST)
                        .forEach { r ->
                            r.approved = true
                            r.updatedAt = Date()
                            repoResourcesReplacement.save(r)
                        }
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateStatusFromTask(taskStatus: Int, taskId: String, taskType: Int) {
        try {
            val task = repo.findByTaskIdAndTaskTypeAndActive(taskId, taskType)
            if (task.isPresent) {
                task.get().updatedBy = getUserLogin()?.id
                task.get().updatedAt = Date()
                task.get().status = taskStatus
                repo.save(task.get())
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun deleteFromTask(taskId: String, taskType: Int) {
        try {
            val findTask = repo.findByTaskIdAndTaskTypeAndActive(taskId, taskType)
            if (findTask.isPresent) {
                findTask.get().active = false
                findTask.get().updatedAt = Date()
                repo.save(findTask.get())
            }

            repoTaskNotification.getByTaskIdAndTaskTypeAndActive(taskId, taskType).forEach { tn ->
                tn.active = false
                tn.updatedAt = Date()
                repoTaskNotification.save(tn)
            }

            if (taskType == TASK_TYPE_LEAVE_REQUEST) {
                repoResourcesReplacement.getByTaskIdAndTaskTypeAndActive(findTask.get().id, TASK_TYPE_LEAVE_REQUEST)
                    .forEach { rr ->
                        rr.active = false
                        rr.updatedAt = Date()
                        repoResourcesReplacement.save(rr)
                    }
            }
        } catch (e: Exception) {
            throw e
        }
    }
}
