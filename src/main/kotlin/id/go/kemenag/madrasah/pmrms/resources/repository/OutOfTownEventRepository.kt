package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.OutOfTownEvent
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface OutOfTownEventRepository : JpaRepository<OutOfTownEvent, String> {

    @Query("from OutOfTownEvent where id = :id and ((:startDate between startDate and endDate) or (:endDate between startDate and endDate)) and active = true and approveStatus in (:approveStatus)")
    fun getByIdStartDateAndDateApproveStatus(
        @Param("id") id: String?,
        @Param("startDate") startDate: Date?,
        @Param("endDate") endDate: Date?,
        @Param("approveStatus") approveStatus: List<Int>?
    ): Optional<OutOfTownEvent>

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<OutOfTownEvent>

}
