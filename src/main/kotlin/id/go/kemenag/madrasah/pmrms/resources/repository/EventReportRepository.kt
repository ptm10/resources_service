package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.EventReport
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface EventReportRepository : JpaRepository<EventReport, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<EventReport>
}
