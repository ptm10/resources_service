package id.go.kemenag.madrasah.pmrms.resources.service

import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.helpers.setCompareData
import id.go.kemenag.madrasah.pmrms.resources.pojo.LogActivity
import id.go.kemenag.madrasah.pmrms.resources.repository.LogActivityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LogActivityService {

    @Autowired
    private lateinit var repo: LogActivityRepository

    fun writeChangeData(newData: Any, oldData: Any?, dataSource: String, ipAdress: String, userId: String) {
        try {
            val objectMapper = ObjectMapper()
            val newDataMappper = setCompareData(objectMapper.writeValueAsString(newData))
            val oldDataMappper = setCompareData(objectMapper.writeValueAsString(oldData))

            newDataMappper.forEach { nd ->
                if (nd.value != oldDataMappper[nd.key]) {
                    var newDataVal = nd.value.toString()
                    var oldDataVal = oldDataMappper[nd.key].toString()
                    if (newDataVal.startsWith("[") || newDataVal.startsWith("{")) {
                        newDataVal = objectMapper.writeValueAsString(nd.value)
                        oldDataVal = objectMapper.writeValueAsString(oldDataMappper[nd.key])
                    }

                    val log = LogActivity(
                        userId = userId,
                        ipAddress = ipAdress,
                        dataSource = dataSource,
                        changeData = nd.key,
                        newData = newDataVal,
                        oldData = oldDataVal
                    )

                    repo.save(log)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
