package id.go.kemenag.madrasah.pmrms.resources.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.util.*


@Suppress("UNCHECKED_CAST", "DEPRECATION")
@Configuration
@EnableSwagger2
class SwaggerConfig {

    @Value("\${swagger.host}")
    private val host: String? = null

    @Value("\${swagger.protocol}")
    private val protocol: String? = null

    @Bean
    fun api(): Docket? {
        return Docket(DocumentationType.SWAGGER_2)
            .host(host)
            .select()
            .apis(RequestHandlerSelectors.basePackage("id.go.kemenag.madrasah.pmrms.resources.controller"))
            .paths(PathSelectors.any())
            .build()
            .useDefaultResponseMessages(false)
            .apiInfo(apiInfo())
            .protocols(setOf(protocol))
            .securitySchemes(listOf(apiKey()) as List<SecurityScheme>?)
            .securityContexts(Collections.singletonList(securityContext()))
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfo(
            "PMRMS API - Resources",
            "Resources for Application PMRMS",
            "1.0",
            "Terms of service",
            Contact("Application PMRMS", "-", "-"),
            "",
            "",
            Collections.emptyList()
        )
    }

    private fun apiKey(): ApiKey {
        return ApiKey("Bearer", "Authorization", "header")
    }

    private fun securityContext(): SecurityContext? {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/.*")).build()
    }

    private fun defaultAuth(): List<SecurityReference?> {
        return listOf(SecurityReference("Bearer", arrayOfNulls(0)))
    }

}
