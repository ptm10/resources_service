package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.ResourceReplacementService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Api(tags = ["Resources Replacement"], description = "Resources Replacement API")
@RestController
@RequestMapping(path = ["resources-replacement"])
class ResourcesReplacementController {

    @Autowired
    private lateinit var service: ResourceReplacementService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @GetMapping(value = ["get-by-task-type-task-id"], produces = ["application/json"])
    fun getByTaskTypeTaskId(
        @RequestParam("taskType") taskType: Int?,
        @RequestParam("taskId") taskId: String?
    ): ResponseEntity<ReturnData> {
        return service.getByTaskTypeTaskId(taskType, taskId)
    }
}
