package id.go.kemenag.madrasah.pmrms.resources.model.response

data class DatatableResponse(
    var content: Any? = null,
    var first: Boolean? = null,
    var last: Boolean? = null,
    var totalElements: Long? = null,
    var totalPages: Int? = null
)
