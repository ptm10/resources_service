package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.Task
import org.springframework.stereotype.Repository

@Repository
class TaskRepositoryNative : BaseRepositoryNative<Task>(Task::class.java)
