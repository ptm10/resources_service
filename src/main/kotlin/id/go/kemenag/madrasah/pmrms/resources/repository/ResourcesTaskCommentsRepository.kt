package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesTaskComments
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ResourcesTaskCommentsRepository : JpaRepository<ResourcesTaskComments, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<ResourcesTaskComments>
}
