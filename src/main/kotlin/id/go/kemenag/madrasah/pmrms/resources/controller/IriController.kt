package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.IriService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Api(tags = ["IRI"], description = "IRI API")
@RestController
@RequestMapping(path = ["iri"])
class IriController {

    @Autowired
    private lateinit var service: IriService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @GetMapping(value = ["get-by-component-id"], produces = ["application/json"])
    fun getByComponentId(@RequestParam("id") id: String?): ResponseEntity<ReturnData> {
        return service.getByComponentId(id)
    }
}
