package id.go.kemenag.madrasah.pmrms.resources.model.response

import id.go.kemenag.madrasah.pmrms.resources.pojo.Resources
import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesSchedule

data class ResourcesScheduleResponse(
    var resources: Resources? = null,
    var leaveDaysCount: Int? = null,
    var leaveDaysRemaining: Int? = null,
    var content: MutableList<ResourcesSchedule> = emptyList<ResourcesSchedule>().toMutableList()
)
