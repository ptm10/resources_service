package id.go.kemenag.madrasah.pmrms.resources.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.constant.*
import id.go.kemenag.madrasah.pmrms.resources.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.resources.helpers.*
import id.go.kemenag.madrasah.pmrms.resources.model.request.*
import id.go.kemenag.madrasah.pmrms.resources.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.pojo.Position
import id.go.kemenag.madrasah.pmrms.resources.pojo.Resources
import id.go.kemenag.madrasah.pmrms.resources.pojo.Role
import id.go.kemenag.madrasah.pmrms.resources.pojo.Users
import id.go.kemenag.madrasah.pmrms.resources.repository.*
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.ResourcesRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST")
@Service
class ResourcesService {

    @Autowired
    private lateinit var repo: ResourcesRepository

    @Autowired
    private lateinit var repoNative: ResourcesRepositoryNative

    @Autowired
    private lateinit var repoRole: RoleRepository

    @Autowired
    private lateinit var repoUserRole: UserRoleRepository

    @Autowired
    private lateinit var repoUsers: UsersRepository

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var repoPosition: PositionRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Autowired
    private lateinit var repoUnit: UnitRepository

    private val staffSupervisiorIdsTemp: MutableList<String> = mutableListOf()

    // datatable
    fun allDatatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            val searchRole = req.paramIn?.filter {
                it.field == "roleId"
            }

            if (!searchRole.isNullOrEmpty()) {
                val searchRoleIds = mutableListOf<String>()
                searchRole.forEach {
                    searchRoleIds.addAll(it.value as List<String>)
                }

                req.paramIn?.add(ParamArray("userId", "string", repoUserRole.getUserIdsByRoleIds(searchRoleIds)))
                req.paramIn?.removeAll(searchRole)
            }

            return responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                val resourcesId = getUserLogin()?.resourcesId
                if (resourcesId.isNullOrEmpty()) {
                    val staffSupervisiorIds = mutableListOf<String>()
                    staffSupervisiorIds.add(resourcesId ?: "")
                    val findStaffSupervisiorIds = getStaffSupervisiorId(resourcesId ?: "", true)
                    if (findStaffSupervisiorIds.isNotEmpty()) {
                        staffSupervisiorIds.addAll(findStaffSupervisiorIds)
                    }

                    req.paramIn?.add(ParamArray("supervisiorId", "string", staffSupervisiorIds))
                } else {
                    req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                return responseSuccess(data = data)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun replacementDatatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val replaceRoleIds: MutableList<String> = mutableListOf()
            getUserRoles().forEach {
                if (it.roleId != null) {
                    val uProle = repoRole.findByIdAndActive(it.role?.supervisiorId)
                    if (uProle.isPresent) {
                        if (uProle.get().id != null) {
                            if (!replaceRoleIds.contains(uProle.get().id)) {
                                replaceRoleIds.add(uProle.get().id!!)
                            }
                        }
                    }

                    // add same role
                    if (!replaceRoleIds.contains(it.roleId)) {
                        replaceRoleIds.add(it.roleId!!)
                    }

                    val buttomRole = repoRole.findBySupervisiorIdAndActive(it.roleId)
                    buttomRole.forEach { br ->
                        if (br.id != null) {
                            if (!replaceRoleIds.contains(br.id)) {
                                replaceRoleIds.add(br.id!!)
                            }
                        }
                    }
                }
            }

            val changeUserIds: List<String> = repoUserRole.getUserIdsByRoleIds(replaceRoleIds).filter {
                it != getUserLogin()?.id
            }
            req.paramIn?.add(ParamArray("user.id", "string", changeUserIds))

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun staff(): ResponseEntity<ReturnData> {
        try {
            var count = 0
            val supervisior = repo.findByUserIdAndActive(getUserLogin()?.id)
            if (supervisior.isPresent) {
                count = repo.countBySupervisiorIdAndActive(supervisior.get().id)
            }
            return responseSuccess(data = count)
        } catch (e: Exception) {
            throw e
        }
    }

    fun getStaffSupervisiorId(supervisiorId: String, first: Boolean = false): List<String> {
        try {
            if (first) {
                staffSupervisiorIdsTemp.clear()
            }
            val findStaffSupervior: List<String> = repo.getBySupervisiorIdAndActive(supervisiorId)
            if (findStaffSupervior.isNotEmpty()) {
                staffSupervisiorIdsTemp.add(supervisiorId)
                findStaffSupervior.forEach {
                    if (repo.countBySupervisiorIdAndActive(it) > 0) {
                        getStaffSupervisiorId(it)
                    }
                }
            }
        } catch (e: Exception) {
            throw e
        }

        return staffSupervisiorIdsTemp.toList()
    }

    fun saveData(request: ResourcesRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(request)
    }

    fun updateData(id: String, request: ResourcesRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveOrUpdate(req: ResourcesRequest, update: Resources? = null): ResponseEntity<ReturnData> { // save or update
        try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val users: Users = validate["users"] as Users

            var resources = Resources()
            var oldData: Resources? = null

            if (update != null) {
                resources = update
                resources.updatedAt = Date()

                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                oldData = objectMapper.readValue(
                    objectMapper.writeValueAsString(resources), Resources::class.java
                ) as Resources
            }

            if (users.roles?.none { fu -> fu.roleId == ROLE_ID_LSP } == true) {
                val position = validate["position"] as Position

                resources.phoneNumber = req.phoneNumber
                resources.userId = users.id
                resources.user = users
                if (!req.supervisiorId.isNullOrEmpty()) {
                    resources.supervisiorId = req.supervisiorId
                } else {
                    resources.supervisiorId = null
                }

                resources.positionId = position.id
                resources.position = position
            } else {
                resources.active = false
            }


            if (users.roles?.any { fu -> fu.roleId == ROLE_ID_PCU } == true) {
                users.componentId = COMPONENT_OTHER_ID
            } else {
                users.provinceId = PROVINCE_OTHER_ID
            }

            repo.save(resources)

            if (update != null) {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        resources,
                        oldData,
                        "resources",
                        ipAdress,
                        userId
                    )
                }.start()
            }
            return responseCreated(data = resources)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: ResourcesRequest, update: Resources? = null): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()
            val findUser = repoUsers.findByIdAndActive(request.userId)
            if (!findUser.isPresent) {
                listMessage.add(ErrorMessage("userId", "User id ${request.userId} $VALIDATOR_MSG_NOT_FOUND"))
            } else {
                rData["users"] = findUser.get()

                val checkUsers = repo.findByUserIdAndActive(findUser.get().id)
                if (checkUsers.isPresent) {
                    val errorMessage = ErrorMessage(
                        "userId",
                        "Staff user ${findUser.get().firstName} ${findUser.get().lastName} $VALIDATOR_MSG_HAS_ADDED"
                    )
                    if (update == null) {
                        listMessage.add(errorMessage)
                    } else {
                        if (update.id != checkUsers.get().id) {
                            listMessage.add(errorMessage)
                        }
                    }
                }

                val unit = repoUnit.findByIdAndActive(request.unitId)
                if (!unit.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "unitId", "Unit id $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    val position: Position

                    val checkPosition = repoPosition.findByNameUnitIdActive(request.position, unit.get().id)

                    position = if (checkPosition.isPresent) {
                        checkPosition.get()
                    } else {
                        repoPosition.save(
                            Position(
                                unitId = unit.get().id, name = request.position?.trim()
                            )
                        )
                    }

                    rData["position"] = position
                }

                if (!request.supervisiorId.isNullOrEmpty()) {
                    val checkSuperVisior = repo.findByIdAndActive(request.supervisiorId)
                    if (!checkSuperVisior.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "supervisiorId",
                                "Supervisor $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun supervisiorByUserId(userId: String?): ResponseEntity<ReturnData> {
        try {
            val user = repoUsers.findByIdAndActive(userId)

            val superVisiorRoleIds = mutableListOf<String>()
            if (user.isPresent) {
                user.get().roles?.forEach {
                    if (!it.role?.supervisiorId.isNullOrEmpty()) {
                        superVisiorRoleIds.add(it.role?.supervisiorId ?: "")
                    }
                }
            }

            val userIds = repoUserRole.getUserIdsByRoleIds(superVisiorRoleIds)

            return responseSuccess(data = repo.getAllByUserIdInAndActive(userIds))
        } catch (e: Exception) {
            throw e
        }
    }

    fun supervisiorByComponentRole(req: SupervisiorByComponentRoleRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateSupervisiorByComponentRole(req)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val userIds = validate["supervisiorUserIds"] as MutableList<String>
            if (userIds.isEmpty()) {
                userIds.add(System.currentTimeMillis().toString())
            }

            val pageRequest = Pagination2Request()

            pageRequest.enablePage = false
            pageRequest.sort?.add(Sort("user.firstName", "asc"))
            pageRequest.sort?.add(Sort("user.lastName", "asc"))
            pageRequest.paramIn?.add(ParamArray("userId", "string", userIds))

            return responseSuccess(data = repoNative.getPage(pageRequest))
        } catch (e: Exception) {
            throw e
        }
    }

    fun supervisiorPcu(req: SupervisiorPcuRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateSupervisiorPcu(req)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val userIds = validate["supervisiorUserIds"] as MutableList<String>
            if (userIds.isEmpty()) {
                userIds.add(System.currentTimeMillis().toString())
            }

            val pageRequest = Pagination2Request()

            pageRequest.enablePage = false
            pageRequest.sort?.add(Sort("user.firstName", "asc"))
            pageRequest.sort?.add(Sort("user.lastName", "asc"))
            pageRequest.paramIn?.add(ParamArray("userId", "string", userIds))

            return responseSuccess(data = repoNative.getPage(pageRequest))
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validateSupervisiorPcu(req: SupervisiorPcuRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val province = repoProvince.findByIdAndActive(req.provinceId)
            if (!province.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "provinceId",
                        "Province id ${req.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            val supervisiorUserIds = mutableListOf<String>()

            if (req.position != POSITION_KETUA_PCU_PROV) {
                supervisiorUserIds.addAll(repoUsers.getIdsByProvinceIdAndActive(req.provinceId))
            } else {
                supervisiorUserIds.addAll(repo.getByPositionInActive(listOf(POSITION_HEAD_PMU, POSITION_SECRETARY_PMU)))
            }

            if (!req.userId.isNullOrEmpty()) {
                supervisiorUserIds.remove(req.userId)
            }

            rData["supervisiorUserIds"] = supervisiorUserIds
            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateSupervisiorByComponentRole(req: SupervisiorByComponentRoleRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val component = repoComponent.findByIdAndActive(req.componentId)
            if (!component.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "componentId",
                        "Component id ${req.componentId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            val supervisiorUserIds = mutableListOf<String>()
            val roles = mutableListOf<Role>()
            req.roleIds?.forEach {
                val checkRole = repoRole.findByIdAndActive(it)
                if (!checkRole.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "roleIds",
                            "Role id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    roles.add(checkRole.get())
                }
            }

            if (roles.isNotEmpty()) {
                val topRoleSelected = roles.sortedByDescending {
                    it.code
                }[0]

                supervisiorUserIds.addAll(
                    repoUserRole.getUserIdsByComponentIdRoleIds(
                        req.componentId,
                        ROLESUPERVISIOR[topRoleSelected.id]
                    )
                )

                // add supervisor other component
                if (req.roleIds?.contains(ROLE_ID_COORDINATOR) == true) {
                    supervisiorUserIds.addAll(
                        repoUserRole.getUserIdsByRoleIds(
                            listOf(
                                ROLE_ID_ADMINSTRATOR,
                                ROLE_ID_COORDINATOR
                            ), 2
                        )
                    )
                }

                if (req.roleIds?.size == 1) {
                    if (req.roleIds?.contains(ROLE_ID_ADMINSTRATOR) == true) {
                        supervisiorUserIds.add(System.currentTimeMillis().toString())
                    }
                }
            }

            if (!req.userId.isNullOrEmpty()) {
                supervisiorUserIds.remove(req.userId)
            }

            rData["supervisiorUserIds"] = supervisiorUserIds
            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    /*    fun positionFixing(): ResponseEntity<ReturnData> {
            try {
                val updated = mutableListOf<Resources>()
                repo.getByActive().forEach {
                    if (it.positionId == null) {
                        if (!it.positionName.isNullOrEmpty()) {
                            val position = repoPosition.findByNameUnitIdActive(it.positionName, UNIT_ID_PMU)
                            if (position.isPresent) {
                                it.positionId = position.get().id
                                repo.save(it)
                                updated.add(it)
                            }
                        }
                    }
                }
                return responseSuccess(data = updated.size)
            } catch (e: Exception) {
                throw e
            }
        }*/
}
