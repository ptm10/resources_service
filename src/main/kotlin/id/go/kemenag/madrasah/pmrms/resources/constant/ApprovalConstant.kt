package id.go.kemenag.madrasah.pmrms.resources.constant

const val APPROVAL_STATUS_REQUEST = 0

const val APPROVAL_STATUS_APPROVE = 1

const val APPROVAL_STATUS_REJECT = 2
