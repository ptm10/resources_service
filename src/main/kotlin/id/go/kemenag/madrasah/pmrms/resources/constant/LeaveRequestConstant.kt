package id.go.kemenag.madrasah.pmrms.resources.constant

val LEAVE_REQUEST_DEFAULT_HOLIDAYS_DAYS = listOf(6, 7)

val LEAVE_REQUEST_MAX_DAY_IN_YEAR = 12
