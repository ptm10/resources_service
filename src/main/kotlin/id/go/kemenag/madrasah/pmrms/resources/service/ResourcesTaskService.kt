package id.go.kemenag.madrasah.pmrms.resources.service

import id.go.kemenag.madrasah.pmrms.resources.constant.*
import id.go.kemenag.madrasah.pmrms.resources.helpers.*
import id.go.kemenag.madrasah.pmrms.resources.model.request.*
import id.go.kemenag.madrasah.pmrms.resources.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.resources.model.users.UsersResources
import id.go.kemenag.madrasah.pmrms.resources.pojo.*
import id.go.kemenag.madrasah.pmrms.resources.repository.*
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.ResourcesTaskRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST", "KotlinConstantConditions")
@Service
class ResourcesTaskService {

    @Autowired
    private lateinit var repo: ResourcesTaskRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoNative: ResourcesTaskRepositoryNative

    @Autowired
    private lateinit var repoPdo: PdoRepository

    @Autowired
    private lateinit var repoIri: IriRepository

    @Autowired
    private lateinit var repoTaskResourcesTag: TaskResourcesTagRepository

    @Autowired
    private lateinit var repoResourcesTaskComments: ResourcesTaskCommentsRepository

    @Autowired
    private lateinit var repoResourcesTaskFiles: ResourcesTaskFilesRepository

    @Autowired
    private lateinit var repoResourcesTaskTags: ResourcesTaskTagsRepository

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var serviceTask: TaskService

    fun datatableSupervisior(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                val supervisior = repoResources.findByUserIdAndActive(getUserLogin()?.id)
                if (supervisior.isPresent) {
                    req.paramIs?.add(ParamSearch("createdBy", "string", supervisior.get().id))
                } else {
                    req.paramIs?.add(ParamSearch("createdBy", "string", System.currentTimeMillis().toString()))
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableStaff(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                val staff = repoResources.findByUserIdAndActive(getUserLogin()?.id)
                if (staff.isPresent) {
                    req.paramIs?.add(ParamSearch("createdFor", "string", staff.get().id))
                } else {
                    req.paramIs?.add(ParamSearch("createdFor", "string", System.currentTimeMillis().toString()))
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                return responseSuccess(data = data)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun save(request: ResourcesTaskRequest): ResponseEntity<ReturnData> {
        try {
            val resources: UsersResources =
                getUserLogin()?.resources ?: return responseBadRequest(message = "User tidak terdaftar sebagai staff")

            val validate = validate(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val cateatedFor = validate["createdFor"] as Resources
            val deadlineDate = validate["deadlineDate"] as Date
            val pdo = validate["pdo"] as Pdo
            val iri = validate["iri"] as Iri
            val files = validate["files"] as List<Files>
            val tags = validate["taskResoucesTags"] as List<TaskResourcesTag>

            val data = ResourcesTask()
            data.title = request.title
            data.description = request.description
            data.createdBy = resources.id
            data.createdFor = cateatedFor.id
            data.createdForResources = cateatedFor
            data.deadlineDate = deadlineDate
            data.pdoId = pdo.id
            data.pdo = pdo
            data.iriId = iri.id
            data.iri = iri

            repo.save(data)

            files.forEach {
                data.files.add(
                    repoResourcesTaskFiles.save(
                        ResourcesTaskFiles(
                            resourcesTaskId = data.id,
                            filesId = it.id,
                            files = it,
                            uploadedFrom = RESOURCES_TASK_CREATOR
                        )
                    )
                )
            }

            tags.forEach {
                data.tags.add(
                    repoResourcesTaskTags.save(
                        ResourcesTaskTags(
                            resourcesTaskId = data.id,
                            taskResourcesTagId = it.id,
                            taskResourcesTag = it
                        )
                    )
                )
            }

            serviceTask.createOrUpdateFromTask(
                taskId = data.id ?: "",
                taskType = TASK_TYPE_RESOURCES_TASK,
                users = getUserLogin(),
                description = "${TASK_TITTLES[TASK_TYPE_RESOURCES_TASK]} ${data.title}",
                receiver = data.createdForResources?.userId,
                update = false,
                createdFor = data.createdForResources?.userId
            )

            return responseCreated(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun delete(request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return try {
            val data = repo.findByIdAndActive(request.id)
            if (data.isPresent) {
                data.get().active = false
                data.get().updatedAt = Date()
                repo.save(data.get())

                serviceTask.deleteFromTask(data.get().id ?: "", TASK_TYPE_RESOURCES_TASK)

                responseSuccess()
            } else {
                responseNotFound()
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun update(id: String, request: ResourcesTaskRequest): ResponseEntity<ReturnData> {
        try {
            val resources: UsersResources =
                getUserLogin()?.resources ?: return responseBadRequest(message = "User tidak terdaftar sebagai staff")

            val findData = repo.findByIdAndActive(id)
            if (findData.isPresent) {
                val data = findData.get()

                if (data.createdBy != resources.id && data.createdFor != resources.id) {
                    return responseBadRequest(message = "User bukan pembuat atau penerima tugas ${data.title}")
                }

                var creator = true
                if (resources.id != data.createdBy) {
                    creator = false
                    data.status = RESOURCES_TASK_ON_PROCCESS
                }

                if (creator) {
                    if (request.title.isNullOrEmpty()) {
                        return responseBadRequest(message = "User bukan penerima tugas ${data.title}")
                    }
                } else {
                    if (!request.title.isNullOrEmpty()) {
                        return responseBadRequest(message = "User bukan pemberi tugas ${data.title}")
                    }
                }

                val validate = validate(request, creator)
                val listMessage = validate["listMessage"] as List<ErrorMessage>
                if (listMessage.isNotEmpty()) {
                    return responseBadRequest(data = listMessage)
                }

                val files = validate["files"] as List<Files>

                var createdFrom = RESOURCES_TASK_CREATOR
                if (!creator) {
                    createdFrom = RESOURCES_TASK_RECEIVER
                }

                val filesBefore = data.files.filter { f -> f.uploadedFrom == createdFrom }

                if (creator) {
                    val tagsBefore = data.tags

                    val deadlineDate = validate["deadlineDate"] as Date
                    val cateatedFor = validate["createdFor"] as Resources
                    val pdo = validate["pdo"] as Pdo
                    val iri = validate["iri"] as Iri
                    val tags = validate["taskResoucesTags"] as List<TaskResourcesTag>

                    data.title = request.title
                    data.createdFor = cateatedFor.id
                    data.createdForResources = cateatedFor
                    data.deadlineDate = deadlineDate
                    data.description = request.description
                    data.pdoId = pdo.id
                    data.pdo = pdo
                    data.iriId = iri.id
                    data.iri = iri
                    data.doneByCreator = request.done

                    tags.forEach { // add tags
                        val find = tagsBefore.filter { f -> f.taskResourcesTagId == it.id }
                        if (find.isEmpty()) {
                            data.tags.add(
                                repoResourcesTaskTags.save(
                                    ResourcesTaskTags(
                                        resourcesTaskId = data.id,
                                        taskResourcesTag = it,
                                        taskResourcesTagId = it.id
                                    )
                                )
                            )
                        }
                    }

                    tagsBefore.forEach { // remove tags
                        val find = tags.filter { f -> f.id == it.taskResourcesTagId }
                        if (find.isEmpty()) {
                            data.tags.remove(it)
                            it.active = false
                            it.updatedAt = Date()
                            repoResourcesTaskTags.save(it)
                        }
                    }

                    if (request.done != null) {
                        data.doneByCreator = request.done
                    }

                    if (data.doneByCreator == false) {
                        if (data.doneByReceiver == true) {
                            data.doneByReceiver = false
                            data.status = RESOURCES_TASK_ON_PROCCESS
                            serviceTask.updateStatusFromTask(
                                TASK_STATUS_ON_PROGRESS,
                                data.id ?: "",
                                TASK_TYPE_RESOURCES_TASK
                            )
                        }
                    } else {
                        data.status = RESOURCES_TASK_DONE_BY_CREATOR
                        serviceTask.approveRejectFromTask(
                            APPROVAL_STATUS_APPROVE,
                            data.id ?: "",
                            TASK_TYPE_RESOURCES_TASK
                        )
                    }

                    serviceTask.createOrUpdateFromTask(
                        taskId = data.id ?: "",
                        taskType = TASK_TYPE_RESOURCES_TASK,
                        users = getUserLogin(),
                        description = "${TASK_TITTLES[TASK_TYPE_RESOURCES_TASK]} ${data.title}",
                        receiver = data.createdForResources?.userId,
                        update = true,
                        createdFor = data.createdForResources?.userId
                    )
                } else {
                    serviceTask.updateStatusFromTask(
                        TASK_STATUS_ON_PROGRESS,
                        data.id ?: "",
                        TASK_TYPE_RESOURCES_TASK
                    )

                    if (request.done != null) {
                        data.doneByReceiver = request.done
                        if (data.doneByReceiver == true) {
                            data.status = RESOURCES_TASK_DONE_BY_RECEIVER
                            serviceTask.updateStatusFromTask(
                                TASK_STATUS_DONE_BY_RECEIVER,
                                data.id ?: "",
                                TASK_TYPE_RESOURCES_TASK
                            )
                        }
                    }

                    if (request.reject == true) {
                        data.status = RESOURCES_TASK_REJECTED
                        data.rejectedMessage = request.rejectedMessage
                        serviceTask.approveRejectFromTask(
                            APPROVAL_STATUS_REJECT,
                            data.id ?: "",
                            TASK_TYPE_RESOURCES_TASK
                        )
                    }
                }

                files.forEach { // add files
                    val find = filesBefore.filter { f -> f.filesId == it.id }
                    if (find.isEmpty()) {
                        data.files.add(
                            repoResourcesTaskFiles.save(
                                ResourcesTaskFiles(
                                    resourcesTaskId = data.id,
                                    uploadedFrom = createdFrom,
                                    filesId = it.id,
                                    files = it
                                )
                            )
                        )
                    }
                }

                filesBefore.forEach { // remove files
                    val find = files.filter { f -> f.id == it.filesId }
                    if (find.isEmpty()) {
                        data.files.remove(it)
                        it.active = false
                        it.updatedAt = Date()
                        repoResourcesTaskFiles.save(it)
                    }
                }

                return responseSuccess(data = repo.save(data))
            } else {
                return responseNotFound()
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(
        request: ResourcesTaskRequest,
        creator: Boolean = true
    ): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()
            if (creator) {
                if (request.title.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "title",
                            "Judul Tugas $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                }

                if (request.createdFor.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "createdFor",
                            "Penerima Tugas $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                }

                var deadlineDate: Date? = null
                val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

                try {
                    deadlineDate = dform.parse(request.deadlineDate)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "deadlineDate",
                            "Tenggat/Deadline Tugas $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                val now = dform.parse(dform.format(Date()))

                if (deadlineDate?.before(now) == true) {
                    listMessage.add(
                        ErrorMessage(
                            "deadlineDate",
                            "Tenggat/Deadline Mulai $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }
                rData["deadlineDate"] = deadlineDate as Date

                val createdForCheck = repoResources.findByIdAndActive(request.createdFor)
                if (!createdForCheck.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "createdForCheck",
                            "Penerima Tugas $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["createdFor"] = createdForCheck.get()
                }

                val pdoCheck = repoPdo.findByIdAndActive(request.pdoId)
                if (!pdoCheck.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "pdoId",
                            "PDO id ${request.pdoId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["pdo"] = pdoCheck.get()
                }

                val iriCheck = repoIri.findByIdAndActive(request.iriId)
                if (!iriCheck.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "iriId",
                            "IRI id ${request.iriId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["iri"] = iriCheck.get()
                }

                val taskResoucesTags = mutableListOf<TaskResourcesTag>()
                request.tagIds?.forEach {
                    val checkTag = repoTaskResourcesTag.findByIdAndActive(it)
                    if (!checkTag.isPresent) {
                        listMessage.add(ErrorMessage("tagIds", "Id Tag $it $VALIDATOR_MSG_NOT_FOUND"))
                    } else {
                        taskResoucesTags.add(checkTag.get())
                    }
                }

                request.newTagNames?.forEach {
                    if (it.isNotEmpty()) {
                        val find = repoTaskResourcesTag.findByNameActive(it)
                        if (!find.isPresent) {
                            taskResoucesTags.add(repoTaskResourcesTag.save(TaskResourcesTag(name = it.trim())))
                        } else {
                            taskResoucesTags.add(find.get())
                        }
                    }
                }

                rData["taskResoucesTags"] = taskResoucesTags
            } else {
                if (request.reject == true) {
                    if (request.rejectedMessage.isNullOrEmpty()) {
                        listMessage.add(ErrorMessage("rejectedMessage", "Alasan Penolakan $VALIDATOR_MSG_REQUIRED"))
                    }
                }
            }

            if (request.createdFor == getUserLogin()?.resourcesId) {
                listMessage.add(ErrorMessage("createdFor", "Pemberi Tugas tidak boleh sama dengan penerima tugas"))
            }

            val files = mutableListOf<Files>()
            request.fileIds?.forEach {
                val chekFile: ReturnDataFiles = RequestHelpers.filesGetById(
                    it,
                    filesUrl,
                    httpServletRequest.getHeader(HEADER_STRING)
                )
                if (!chekFile.success!!) {
                    listMessage.add(ErrorMessage("fileIds", chekFile.message))
                } else {
                    chekFile.data?.let { it1 -> files.add(it1) }
                }
            }
            rData["files"] = files

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun createComment(request: ResourcesTaskCommentRequest): ResponseEntity<ReturnData> {
        return try {
            val task = repo.findByIdAndActive(request.resourcesTaskId)
            if (task.isPresent) {
                responseSuccess(
                    data = repoResourcesTaskComments.save(
                        ResourcesTaskComments(
                            resourcesTaskId = task.get().id,
                            message = request.comment,
                            createdFrom = request.createdFrom
                        )
                    )
                )
            } else {
                responseNotFound()
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun deleteComment(request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return try {
            val data = repoResourcesTaskComments.findByIdAndActive(request.id)
            if (data.isPresent) {
                data.get().active = false
                data.get().updatedAt = Date()
                repoResourcesTaskComments.save(data.get())

                responseSuccess()
            } else {
                responseNotFound()
            }
        } catch (e: Exception) {
            throw e
        }
    }
}
