package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskResourcesTag
import org.springframework.stereotype.Repository

@Repository
class TagResourcesTagRepositoryNative : BaseRepositoryNative<TaskResourcesTag>(TaskResourcesTag::class.java)
