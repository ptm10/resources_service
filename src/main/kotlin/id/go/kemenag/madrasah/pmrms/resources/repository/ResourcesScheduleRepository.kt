package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.constant.APPROVAL_STATUS_APPROVE
import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesSchedule
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface ResourcesScheduleRepository : JpaRepository<ResourcesSchedule, String> {

    fun findByResourcesIdAndTaskIdAndTaskTypeAndActive(
        resourceId: String?,
        taskId: String?,
        taskType: Int?,
        active: Boolean? = true
    ): Optional<ResourcesSchedule>

    @Query("select sum(rs.daysLength) as days from ResourcesSchedule rs where rs.resourcesId = :resourcesId and rs.taskType = :taskType and rs.approveStatus = :approveStatus and rs.active = :active")
    fun sumDaysLengthByResourcesTaskTypeApproveStatusActive(
        resourcesId: String?,
        taskType: Int?,
        approveStatus: Int? = APPROVAL_STATUS_APPROVE,
        active: Boolean? = true
    ): Int ?
}
