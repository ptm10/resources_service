package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesTask
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ResourcesTaskRepository : JpaRepository<ResourcesTask, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<ResourcesTask>
}
