package id.go.kemenag.madrasah.pmrms.resources.model.request

data class ResourcesTaskRequest(

    var title: String? = null,

    var createdFor: String? = null,

    var deadlineDate: String? = null,

    var description: String? = null,

    var pdoId: String? = null,

    var iriId: String? = null,

    var fileIds: List<String>? = null,

    var tagIds: List<String>? = null,

    var newTagNames: List<String>? = null,

    var done: Boolean? = null,

    var reject: Boolean? = false,

    var rejectedMessage: String? = null
)
