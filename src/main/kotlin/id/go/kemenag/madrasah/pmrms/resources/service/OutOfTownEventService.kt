package id.go.kemenag.madrasah.pmrms.resources.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.constant.*
import id.go.kemenag.madrasah.pmrms.resources.helpers.*
import id.go.kemenag.madrasah.pmrms.resources.model.request.*
import id.go.kemenag.madrasah.pmrms.resources.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.model.users.UsersResources
import id.go.kemenag.madrasah.pmrms.resources.pojo.*
import id.go.kemenag.madrasah.pmrms.resources.repository.*
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.OutOfTimeEventRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Service
@Suppress("UNCHECKED_CAST")
class OutOfTownEventService {

    @Autowired
    private lateinit var repo: OutOfTownEventRepository

    @Autowired
    private lateinit var repoNative: OutOfTimeEventRepositoryNative

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoEventOutOfTown: EventOutOfTownRepository

    @Autowired
    private lateinit var repoTransportation: TransportationRepository

    @Autowired
    private lateinit var repoOutOfTownEventResources: OutOfTownEventResourcesRepository

    @Autowired
    private lateinit var repoLeaveRequest: LeaveRequestRepository

    @Autowired
    private lateinit var serviceTask: TaskService

    @Autowired
    private lateinit var repoResourcesReplacement: ResourcesReplacementRepository

    @Autowired
    private lateinit var serviceResourcesSchedule: ResourcesScheduleService

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var logActivityService: LogActivityService

    fun datatableSupervisior(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                val supervisior = repoResources.findByUserIdAndActive(getUserLogin()?.id)
                if (supervisior.isPresent) {
                    val supervisiorIds = mutableListOf<String>()
                    supervisiorIds.add(supervisior.get().id ?: "")

                    // add task from replacement
                    val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                    repoResourcesReplacement.findByResourcesIdDateApproved(
                        supervisior.get().id, dform.parse(dform.format(Date()))
                    ).forEach {
                        if (!supervisiorIds.contains(it.resourcesReplaceId)) {
                            supervisiorIds.add(it.resourcesReplaceId ?: "")
                        }
                    }

                    req.paramIn?.add(ParamArray("resources.supervisiorId", "string", supervisiorIds))
                } else {
                    req.paramIs?.add(
                        ParamSearch(
                            "resources.supervisiorId",
                            "string",
                            System.currentTimeMillis().toString()
                        )
                    )
                }
            } else {
                req.paramNotIn?.add(ParamArray("resources.userId", "string", listOf(getUserLogin()?.id ?: "")))
            }

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableStaff(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val ids = mutableListOf<String>()

            val staff = repoResources.findByUserIdAndActive(getUserLogin()?.id)
            if (staff.isPresent) {
                repoOutOfTownEventResources.getByResourcesIdAndActive(staff.get().id).forEach {
                    if (!ids.contains(it.outOfTownEventId)) {
                        ids.add(it.outOfTownEventId ?: "")
                    }
                }
            }

            if (ids.isNotEmpty()) {
                req.paramIn?.add(ParamArray("id", "string", ids))
            } else {
                req.paramIs?.add(
                    ParamSearch(
                        "id",
                        "string",
                        System.currentTimeMillis().toString()
                    )
                )
            }

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveData(request: OutOfTownRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(request)
    }

    private fun saveOrUpdate(request: OutOfTownRequest, update: OutOfTownEvent? = null): ResponseEntity<ReturnData> {
        try {
            val resources: UsersResources =
                getUserLogin()?.resources ?: return responseBadRequest(message = "User tidak terdaftar sebagai staff")

            val validate = validate(request, resources)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val event: EventOutOfTown = validate["event"] as EventOutOfTown
            val transportation: Transportation = validate["transportation"] as Transportation
            val startDate: Date = validate["startDate"] as Date
            val endDate: Date = validate["endDate"] as Date
            val transportationStartTime: Date = validate["transportationStartTime"] as Date
            val transportationEndTime: Date = validate["transportationEndTime"] as Date
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val resourcesDetails: List<Resources> = validate["resourcesDetails"] as List<Resources>

            var data = OutOfTownEvent()
            var oldData: OutOfTownEvent? = null

            if (update != null) {
                data = update
                data.updatedAt = Date()

                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                oldData = objectMapper.readValue(
                    objectMapper.writeValueAsString(data), OutOfTownEvent::class.java
                ) as OutOfTownEvent
            }

            resourcesDetails.forEach { // check out of town request user
                repoOutOfTownEventResources.getByResourcesIdAndActive(it.id).forEach { rr ->
                    val checkOutOfTownEvent = repo.getByIdStartDateAndDateApproveStatus(
                        rr.outOfTownEventId, startDate, endDate, listOf(
                            APPROVAL_STATUS_REQUEST, APPROVAL_STATUS_APPROVE
                        )
                    )

                    if (checkOutOfTownEvent.isPresent) {
                        var duplicate = true
                        if (update != null) {
                            if (update.id != checkOutOfTownEvent.get().id) {
                                duplicate = false
                            }
                        }

                        if (duplicate) {
                            val messageResources =
                                "User ${it.user?.firstName} ${it.user?.lastName} sudah mengajukan dinas perjalanan luar kota pada tanggal ${
                                    dateFormat.format(
                                        checkOutOfTownEvent.get().startDate
                                    )
                                } sampai ${
                                    dateFormat.format(
                                        checkOutOfTownEvent.get().endDate
                                    )
                                }"

                            return responseBadRequest(message = messageResources)
                        }
                    }
                }
            }

            repo.save(data.apply {
                this.resourcesId = resources.id
                this.eventId = request.eventId
                this.event = event
                this.startDate = startDate
                this.endDate = endDate
                this.daysLength = getDaysBetween(startDate, endDate, dateFormat) + 1
                this.description = request.description
                this.address = request.address
                this.transportationId = request.transportationId
                this.transportation = transportation
                this.origin = request.origin
                this.destination = request.destination
                this.transportationStartTime = transportationStartTime
                this.transportationEndTime = transportationEndTime
                this.transportationCode = request.transportationCode
            })

            val details = mutableListOf<OutOfTownEventResources>()
            resourcesDetails.forEach {
                details.add(
                    OutOfTownEventResources(
                        resourcesId = it.id,
                        outOfTownEventId = data.id
                    )
                )
            }

            if (update != null) {
                repoOutOfTownEventResources.deleteAllByOutOfTownEventId(data.id)
            }

            repoOutOfTownEventResources.saveAll(details)

            data.details = details

            val dformNotif: DateFormat = SimpleDateFormat("dd MMMM yyyy")
            val taskDescription =
                "Mengajukan Kegiatan di Luar Kota mulai tanggal ${dformNotif.format(startDate)} sampai ${
                    dformNotif.format(endDate)
                }"

            var supervisiorUserId: String? = null
            if (!resources.supervisiorId.isNullOrEmpty()) {
                val supervisior = repoResources.findByIdAndActive(resources.supervisiorId)
                if (supervisior.isPresent) {
                    supervisiorUserId = supervisior.get().userId
                }
            }

            serviceTask.createOrUpdateFromTask(
                taskId = data.id ?: "",
                taskType = TASK_TYPE_OUT_OF_TOWN_REQUEST,
                users = getUserLogin(),
                description = taskDescription,
                receiver = supervisiorUserId,
                update = update != null
            )


            resourcesDetails.forEach {
                var scheDuleDesciption =
                    "Peserta : ${it.user?.firstName} ${it.user?.lastName} (${data.details?.size} orang)"
                scheDuleDesciption += ", Nama Kegiatan : ${data.event?.name}"
                scheDuleDesciption += ", Agenda Kegiatan : ${data.description}"

                val schedule = ResourcesSchedule(
                    resourcesId = it.id,
                    taskId = data.id,
                    taskType = TASK_TYPE_OUT_OF_TOWN_REQUEST,
                    approveStatus = data.approveStatus,
                    startDate = data.startDate,
                    endDate = data.endDate,
                    daysLength = data.daysLength,
                    description = scheDuleDesciption
                )

                serviceResourcesSchedule.createFromTask(schedule, update != null)
            }

            if (supervisiorUserId == null) {
                approve(
                    ApproveRequest(
                        id = data.id,
                        approveStatus = APPROVAL_STATUS_APPROVE
                    )
                )
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "out_of_town_event",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: OutOfTownRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            if (update.get().approveStatus == APPROVAL_STATUS_REQUEST) {
                saveOrUpdate(request, update.get())
            } else {
                responseBadRequest(VALIDATOR_MSG_CANNOT_CHANGE_DATA)
            }
        } else {
            responseNotFound()
        }
    }

    fun approve(request: ApproveRequest): ResponseEntity<ReturnData> {
        try {
            val resources = repoResources.findByUserIdAndActive(getUserLogin()?.id)
            if (!resources.isPresent) {
                return responseBadRequest(message = "User tidak terdaftar sebagai staff")
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            val validate = validateApprove(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            find.get().approveStatus = request.approveStatus
            find.get().rejectedMessage = request.rejectedMessage
            find.get().updatedBy = resources.get().id
            find.get().updatedAt = Date()
            repo.save(find.get())

            serviceTask.approveRejectFromTask(
                request.approveStatus ?: 0,
                find.get().id ?: "",
                TASK_TYPE_OUT_OF_TOWN_REQUEST
            )

            find.get().details?.forEach {
                serviceResourcesSchedule.approveFromTask(
                    request.approveStatus,
                    it.resourcesId,
                    find.get().id,
                    TASK_TYPE_OUT_OF_TOWN_REQUEST
                )
            }

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                return responseSuccess(data = data)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun delete(request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return try {
            val data = repo.findByIdAndActive(request.id)
            if (data.isPresent) {
                data.get().active = false
                data.get().updatedAt = Date()
                repo.save(data.get())

                serviceTask.deleteFromTask(data.get().id ?: "", TASK_TYPE_OUT_OF_TOWN_REQUEST)

                data.get().details?.forEach {
                    it.updatedAt = Date()
                    it.active = false
                    repoOutOfTownEventResources.save(it)

                    serviceResourcesSchedule.deleteFromTask(
                        it.resourcesId,
                        it.outOfTownEventId,
                        TASK_TYPE_OUT_OF_TOWN_REQUEST
                    )
                }

                responseSuccess()
            } else {
                responseNotFound()
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validateApprove(request: ApproveRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                if (request.rejectedMessage.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "rejectedMessage",
                            "Alasan Penolakan $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validate(
        request: OutOfTownRequest,
        resources: UsersResources,
    ): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            var startDate: Date? = null
            var endDate: Date? = null
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                startDate = dateFormat.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate",
                        "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }
            rData["startDate"] = startDate as Date

            try {
                endDate = dateFormat.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate",
                        "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }
            rData["endDate"] = endDate as Date

            val findEvent = repoEventOutOfTown.findByIdAndActive(request.eventId)
            if (!findEvent.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventId",
                        "Kegiatan id ${request.eventId} $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            } else {
                rData["event"] = findEvent.get()
            }

            val findTransportation = repoTransportation.findByIdAndActive(request.transportationId)
            if (!findTransportation.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "transportationId",
                        "Moda Transportasi id ${request.eventId} $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            } else {
                rData["transportation"] = findTransportation.get()
            }


            var transportationStartTime: Date? = null
            var transportationEndTime: Date? = null
            val dateTimeFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")

            try {
                transportationStartTime = dateTimeFormat.parse(request.transportationStartTime)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate",
                        "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }
            rData["transportationStartTime"] = transportationStartTime as Date

            try {
                transportationEndTime = dateTimeFormat.parse(request.transportationEndTime)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate",
                        "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }
            rData["transportationEndTime"] = transportationEndTime as Date

            if (!request.resourcesIds.contains(resources.id)) {
                request.resourcesIds.add(resources.id ?: "")
            }

            val resourcesDetails = mutableListOf<Resources>()

            request.resourcesIds.forEach { rf ->
                val checkResources = repoResources.findByIdAndActive(rf)
                if (!checkResources.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "endDate",
                            "Staff id $rf $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else { // check leave request
                    resourcesDetails.add(checkResources.get())

                    val checkResourcesLeave = repoLeaveRequest.findByResourcesIdStartDateAndDate(
                        rf, startDate, endDate, listOf(
                            APPROVAL_STATUS_REQUEST, APPROVAL_STATUS_APPROVE
                        )
                    )
                    if (checkResourcesLeave.isPresent) {
                        val messageResourceLeave =
                            "User ${checkResources.get().user?.firstName} ${checkResources.get().user?.lastName} sudah mengajukan izin ${checkResourcesLeave.get().leaveRequestType?.name} pada tanggal ${
                                dateFormat.format(
                                    checkResourcesLeave.get().startDate
                                )
                            } sampai ${
                                dateFormat.format(
                                    checkResourcesLeave.get().endDate
                                )
                            }"

                        listMessage.add(
                            ErrorMessage(
                                "resourcesIds",
                                messageResourceLeave
                            )
                        )
                    }
                }
            }

            rData["resourcesDetails"] = resourcesDetails

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }
}
