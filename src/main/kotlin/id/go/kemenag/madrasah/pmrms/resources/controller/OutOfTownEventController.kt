package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.ApproveRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.DeleteDataRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.OutOfTownRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.OutOfTownEventService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Out Of Town Event"], description = "Out Of Town Event API")
@RestController
@RequestMapping(path = ["out-of-town-event"])
class OutOfTownEventController {

    @Autowired
    private lateinit var service: OutOfTownEventService

    @PostMapping(value = ["datatable-supervisior"], produces = ["application/json"])
    fun datatableSupervisior(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableSupervisior(req)
    }

    @PostMapping(value = ["datatable-staff"], produces = ["application/json"])
    fun datatableStaff(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableStaff(req)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun save(@Valid @RequestBody request: OutOfTownRequest): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(@PathVariable id: String, @Valid @RequestBody request: OutOfTownRequest): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PutMapping(value = ["approve"], produces = ["application/json"])
    fun approve(@Valid @RequestBody request: ApproveRequest): ResponseEntity<ReturnData> {
        return service.approve(request)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @DeleteMapping(value = [""], produces = ["application/json"])
    fun delete(@Valid @RequestBody request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return service.delete(request)
    }

}
