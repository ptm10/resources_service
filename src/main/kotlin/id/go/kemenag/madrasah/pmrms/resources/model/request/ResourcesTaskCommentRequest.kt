package id.go.kemenag.madrasah.pmrms.resources.model.request

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ResourcesTaskCommentRequest(

    @field:NotEmpty
    var resourcesTaskId: String? = null,

    @field:NotEmpty
    var comment: String? = null,

    @field:NotNull
    var createdFrom: Int? = null
)
