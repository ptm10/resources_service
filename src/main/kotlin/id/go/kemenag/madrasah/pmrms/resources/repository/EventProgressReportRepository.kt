package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.EventProgressReport
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface EventProgressReportRepository : JpaRepository<EventProgressReport, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<EventProgressReport>
}
