package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskResourcesTag
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface TaskResourcesTagRepository : JpaRepository<TaskResourcesTag, String> {

    @Query("FROM TaskResourcesTag WHERE LOWER(TRIM(:name)) = LOWER(TRIM(name)) AND active = :active")
    fun findByNameActive(
        @Param("name") name: String?,
        @Param("active") active: Boolean? = true
    ): Optional<TaskResourcesTag>

    fun findByIdAndActive(id: String? = null, active: Boolean? = true): Optional<TaskResourcesTag>
}
