package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.support.PageableExecutionUtils
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager
import javax.persistence.Query

@Suppress("UNCHECKED_CAST")
open class BaseRepositoryNative<T>(_class: Class<T>?) {

    private val collection = _class?.simpleName

    @Autowired
    private lateinit var em: EntityManager

    @Transactional
    open fun getPage(req: Pagination2Request): Page<T>? {
        try {
            var queryStr = "SELECT s FROM $collection s WHERE s.active = true"
            if (!req.paramLike.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramLike?.forEachIndexed { index, element ->
                    queryStr += "LOWER(s.${element.field}) LIKE LOWER(CONCAT('%',:${replaceParamQuery(element.field ?: "")},'%'))"

                    if ((index + 1) < req.paramLike!!.size) {
                        queryStr += " OR "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramIs.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramIs?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} = :${replaceParamQuery(element.field ?: "")}"

                    if ((index + 1) < req.paramIs!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramIn.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramIn?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} IN(:${replaceParamQuery(element.field ?: "")})"

                    if ((index + 1) < req.paramIn!!.size) {
                        queryStr += " OR "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramNotIn.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramNotIn?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} NOT IN(:${replaceParamQuery(element.field ?: "")})"

                    if ((index + 1) < req.paramNotIn!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramDateBetween.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramDateBetween?.forEachIndexed { index, element ->
                    queryStr += "to_char(s.${element.field}, 'YYYY-MM-DD') BETWEEN :start${replaceParamQuery(element.field ?: "")} AND :end${
                        replaceParamQuery(
                            element.field ?: ""
                        )
                    }"

                    if ((index + 1) < req.paramDateBetween!!.size) {
                        queryStr += " OR "
                    }
                }

                queryStr += ")"
            }

            if (!req.sort.isNullOrEmpty()) {
                queryStr += " ORDER BY "

                req.sort?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} ${element.direction}"

                    if ((index + 1) < req.sort!!.size) {
                        queryStr += ", "
                    }
                }
            }

            val query: Query = em.createQuery(queryStr, Any::class.java)

            req.paramLike?.forEach {
                query.setParameter(replaceParamQuery(it.field ?: ""), it.value)
            }

            req.paramIs?.forEach {
                query.setParameter(replaceParamQuery(it.field ?: ""), parseSearchValue(it.dataType, it.value))
            }

            req.paramIn?.forEach {
                query.setParameter(replaceParamQuery(it.field ?: ""), parseSearchValueArray(it.dataType, it.value))
            }

            req.paramNotIn?.forEach {
                query.setParameter(replaceParamQuery(it.field ?: ""), parseSearchValueArray(it.dataType, it.value))
            }


            req.paramDateBetween?.forEach {
                val paramStart = "start${replaceParamQuery(it.field ?: "")}"
                val paramEnd = "end${replaceParamQuery(it.field ?: "")}"

                query.setParameter(paramStart, it.startDate)
                query.setParameter(paramEnd, it.endDate)
            }

            val size: Long = query.resultList.size.toLong()

            if (req.enablePage == true) {
                query.firstResult = req.page!! * req.size!!
                query.maxResults = req.size!!
            }

            val list: List<T> = query.resultList as List<T>

            var pageable: Pageable = PageRequest.of(req.page!!, req.size!!)
            if (req.enablePage != true) {
                pageable = Pageable.unpaged()
            }

            return PageableExecutionUtils.getPage(
                list,
                pageable
            ) {
                size
            }
        } catch (e: Exception) {
            throw e
        }
    }
}

private fun replaceParamQuery(param: String): String {
    return param.replace(".", "_")
}

private fun parseSearchValue(dataType: String?, value: Any?): Any {
    return when (dataType) {
        "int" -> {
            Integer.valueOf(value.toString())
        }
        "boolean" -> {
            value.toString().toBoolean()
        }
        else -> {
            value.toString()
        }
    }
}

private fun parseSearchValueArray(dataType: String?, value: List<Any>?): List<Any> {
    val paramSearch = mutableListOf<Any>()

    value?.forEach {
        when (dataType) {
            "int" -> {
                paramSearch.add(Integer.valueOf(it.toString()))
            }
            "boolean" -> {
                paramSearch.add(it.toString().toBoolean())
            }
            else -> {
                paramSearch.add(it.toString())
            }
        }
    }

    return paramSearch

}


