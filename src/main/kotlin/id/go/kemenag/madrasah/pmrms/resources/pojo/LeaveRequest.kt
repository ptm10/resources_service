package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "leave_request", schema = "public")
data class LeaveRequest(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "resources_id")
    var resourcesId: String? = null,

    @ManyToOne
    @JoinColumn(name = "resources_id", insertable = false, updatable = false, nullable = true)
    var resources: Resources? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "days_length")
    var daysLength: Int? = null,

    @Column(name = "works_days")
    var worksDays: Int? = null,

    @Column(name = "holidays")
    var holidays: Int? = null,

    @Column(name = "leave_request_type_id")
    var leaveRequestTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "leave_request_type_id", insertable = false, updatable = false, nullable = true)
    var leaveRequestType: LeaveRequestType? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "address")
    var address: String? = null,

    @Column(name = "approve_status")
    var approveStatus: Int? = 0,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @Column(name = "rejected_message")
    var rejectedMessage: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
