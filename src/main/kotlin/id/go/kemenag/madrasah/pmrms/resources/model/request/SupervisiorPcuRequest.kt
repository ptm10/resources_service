package id.go.kemenag.madrasah.pmrms.resources.model.request


class SupervisiorPcuRequest(

    var provinceId: String? = null,

    var userId: String? = null,

    var position: String? = null
)
