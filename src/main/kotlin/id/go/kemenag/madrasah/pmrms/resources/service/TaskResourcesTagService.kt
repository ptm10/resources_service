package id.go.kemenag.madrasah.pmrms.resources.service

import id.go.kemenag.madrasah.pmrms.resources.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.pojo.TaskResourcesTag
import id.go.kemenag.madrasah.pmrms.resources.repository.TaskResourcesTagRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.TagResourcesTagRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TaskResourcesTagService {

    @Autowired
    private lateinit var repo: TaskResourcesTagRepository

    @Autowired
    private lateinit var repoNative: TagResourcesTagRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun save(request: TaskResourcesTag): ResponseEntity<ReturnData> {
        try {
            val find = repo.findByNameActive(request.name)
            if (!find.isPresent) {
                repo.save(TaskResourcesTag(name = request.name?.trim()))
            }
            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }
}
