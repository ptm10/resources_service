package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.Resources
import org.springframework.stereotype.Repository

@Repository
class ResourcesRepositoryNative : BaseRepositoryNative<Resources>(Resources::class.java)
