package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.ResourcesTaskTags
import org.springframework.data.jpa.repository.JpaRepository

interface ResourcesTaskTagsRepository : JpaRepository<ResourcesTaskTags, String> {
}
