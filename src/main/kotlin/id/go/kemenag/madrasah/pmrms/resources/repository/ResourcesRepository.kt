package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Resources
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface ResourcesRepository : JpaRepository<Resources, String> {

    fun findByUserIdAndActive(userId: String?, active: Boolean = true): Optional<Resources>

    fun countBySupervisiorIdAndActive(supervisiorId: String?, active: Boolean = true): Int

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<Resources>

    fun findBySupervisiorIdAndActive(supervisiorId: String?, active: Boolean = true): List<Resources>

    @Query("select r.id from Resources r where r.supervisiorId = :supervisiorId and r.active = :active")
    fun getBySupervisiorIdAndActive(
        @Param("supervisiorId") supervisiorId: String?,
        @Param("active") active: Boolean = true
    ): List<String>

    fun getAllByUserIdInAndActive(userIds: List<String>, active: Boolean? = true): List<Resources>

    @Query("select r.userId from Resources r where r.position.name in (:positions) and r.active = :active")
    fun getByPositionInActive(positions: List<String>, active: Boolean? = true): List<String>

    fun getByActive(active: Boolean = true): List<Resources>
}
