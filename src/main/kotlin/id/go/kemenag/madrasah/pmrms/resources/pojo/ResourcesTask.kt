package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.resources.constant.RESOURCES_TASK_STATUS_NEW
import id.go.kemenag.madrasah.pmrms.resources.constant.TASK_STATUS_NEW
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "resources_task", schema = "public")
data class ResourcesTask(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "title")
    var title: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByResources: Resources? = null,

    @Column(name = "created_for")
    var createdFor: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_for", insertable = false, updatable = false, nullable = true)
    var createdForResources: Resources? = null,

    @Column(name = "pdoId")
    var pdoId: String? = null,

    @ManyToOne
    @JoinColumn(name = "pdoId", insertable = false, updatable = false, nullable = true)
    var pdo: Pdo? = null,

    @Column(name = "iri_id")
    var iriId: String? = null,

    @ManyToOne
    @JoinColumn(name = "iri_id", insertable = false, updatable = false, nullable = true)
    var iri: Iri? = null,

    @Column(name = "deadline_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var deadlineDate: Date? = null,

    @Column(name = "status")
    var status: Int? = RESOURCES_TASK_STATUS_NEW,

    @Column(name = "done_by_creator")
    var doneByCreator: Boolean? = false,

    @Column(name = "done_by_receiver")
    var doneByReceiver: Boolean? = false,

    @Column(name = "rejected_message")
    var rejectedMessage: String? = null,

    @OneToMany(mappedBy = "resourcesTaskId")
    @Where(clause = "active = true")
    @OrderBy("createdAt")
    var files: MutableList<ResourcesTaskFiles> = emptyList<ResourcesTaskFiles>().toMutableList(),

    @OneToMany(mappedBy = "resourcesTaskId")
    @Where(clause = "active = true")
    @OrderBy("createdAt")
    var comments: MutableList<ResourcesTaskComments> = emptyList<ResourcesTaskComments>().toMutableList(),

    @OneToMany(mappedBy = "resourcesTaskId")
    @Where(clause = "active = true")
    @OrderBy("createdAt")
    var tags: MutableList<ResourcesTaskTags> = emptyList<ResourcesTaskTags>().toMutableList(),

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
