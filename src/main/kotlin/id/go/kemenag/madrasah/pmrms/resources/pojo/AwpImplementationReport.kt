package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "awp_implementation_report", schema = "public")
data class AwpImplementationReport(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "status")
    var status: Int? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
