package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.LeaveRequestType
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface LeaveRequestTypeRepository : JpaRepository<LeaveRequestType, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<LeaveRequestType>
}
