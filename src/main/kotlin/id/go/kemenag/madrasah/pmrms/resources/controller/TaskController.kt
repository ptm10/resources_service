package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.TaskUpdateByTypeRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.TaskUpdateRequest
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.TaskService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Task"], description = "Task API")
@RestController
@RequestMapping(path = ["task"])
class TaskController {

    @Autowired
    private lateinit var service: TaskService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = ["datatable-creator"], produces = ["application/json"])
    fun datatableCreator(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableCreator(req)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun updateData(
        @PathVariable id: String,
        @Valid @RequestBody request: TaskUpdateRequest
    ): ResponseEntity<ReturnData> {
        return service.update(id, request)
    }

    @PutMapping(value = ["update-by-task-id-task-type"], produces = ["application/json"])
    fun updateByTaskIdTaskType(
        @Valid @RequestBody req: TaskUpdateByTypeRequest
    ): ResponseEntity<ReturnData> {
        return service.updateByTaskIdTaskType(req)
    }
}
