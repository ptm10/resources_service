package id.go.kemenag.madrasah.pmrms.resources.model.response

data class ErrorMessage(
    var key: String? = null,
    var message: String? = null
)
