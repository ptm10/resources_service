package id.go.kemenag.madrasah.pmrms.resources.model.request

import id.go.kemenag.madrasah.pmrms.resources.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class DocumentRequest(

    @field:NotEmpty(message = "Id Awp $VALIDATOR_MSG_REQUIRED")
    var awpId: String? = null,

    @field:NotEmpty(message = "Attachment Type $VALIDATOR_MSG_REQUIRED")
    var attachmentTypeId: String? = null,

    @field:NotEmpty(message = "File Ids $VALIDATOR_MSG_REQUIRED")
    var fileIds: List<String>? = null
)
