package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Role
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RoleRepository : JpaRepository<Role, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Role>

    fun findBySupervisiorIdAndActive(supervisiorId: String?, active: Boolean? = true): List<Role>
}
