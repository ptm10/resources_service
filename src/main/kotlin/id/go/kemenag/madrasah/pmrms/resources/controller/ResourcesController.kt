package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.ResourcesRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.SupervisiorByComponentRoleRequest
import id.go.kemenag.madrasah.pmrms.resources.model.request.SupervisiorPcuRequest
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.ResourcesService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Resources"], description = "Resources API")
@RestController
@RequestMapping(path = ["resources"])
class ResourcesController {

    @Autowired
    private lateinit var service: ResourcesService

    @PostMapping(value = ["all-datatable"], produces = ["application/json"])
    fun allDatatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.allDatatable(req)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun save(@RequestBody req: ResourcesRequest): ResponseEntity<ReturnData> {
        return service.saveData(req)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(@PathVariable id: String, @Valid @RequestBody request: ResourcesRequest): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PostMapping(value = ["replacement-datatable"], produces = ["application/json"])
    fun replacementDatatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.replacementDatatable(req)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @GetMapping(value = ["staff"], produces = ["application/json"])
    fun staff(): ResponseEntity<ReturnData> {
        return service.staff()
    }

    @GetMapping(value = ["supervisior-by-user-id"], produces = ["application/json"])
    fun supervisiorByUserId(@RequestParam("userId") userId: String?): ResponseEntity<ReturnData> {
        return service.supervisiorByUserId(userId)
    }

    @PostMapping(value = ["supervisior-by-component-role"], produces = ["application/json"])
    fun supervisiorByComponentRole(@RequestBody req: SupervisiorByComponentRoleRequest): ResponseEntity<ReturnData> {
        return service.supervisiorByComponentRole(req)
    }

    @PostMapping(value = ["supervisior-pcu"], produces = ["application/json"])
    fun supervisiorPcu(@RequestBody req: SupervisiorPcuRequest): ResponseEntity<ReturnData> {
        return service.supervisiorPcu(req)
    }

    /*@PostMapping(value = ["position-fixing"], produces = ["application/json"])
    fun positionFixing(): ResponseEntity<ReturnData> {
        return service.positionFixing()
    }*/
}
