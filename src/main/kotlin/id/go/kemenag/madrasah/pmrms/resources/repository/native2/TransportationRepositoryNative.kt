package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.Transportation
import org.springframework.stereotype.Repository

@Repository
class TransportationRepositoryNative : BaseRepositoryNative<Transportation>(Transportation::class.java)
