package id.go.kemenag.madrasah.pmrms.resources.pojo

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "event", schema = "public")
data class EventId(
    @Id
    var id: String? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
