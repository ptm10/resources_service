package id.go.kemenag.madrasah.pmrms.resources.repository

import id.go.kemenag.madrasah.pmrms.resources.pojo.Component
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface ComponentRepository : JpaRepository<Component, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<Component>
}
