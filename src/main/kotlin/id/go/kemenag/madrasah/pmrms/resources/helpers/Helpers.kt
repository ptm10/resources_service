@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "UNCHECKED_CAST")

package id.go.kemenag.madrasah.pmrms.resources.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.resources.model.users.Users
import id.go.kemenag.madrasah.pmrms.resources.model.users.UsersRole
import org.springframework.core.env.Environment
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.support.WebApplicationContextUtils
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import java.text.DateFormat
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*
import javax.servlet.http.HttpServletRequest


fun getUserLogin(): Users? {
    return try {
        val principal = SecurityContextHolder.getContext().authentication.principal as Any
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.readValue(principal.toString(), Users::class.java)
    } catch (e: Exception) {
        null
    }
}

fun getUserRoles(): MutableSet<UsersRole> {
    var roles: MutableSet<UsersRole> = mutableSetOf()
    try {
        getUserLogin()?.let {
            if (!it.roles.isNullOrEmpty()) {
                roles = getUserLogin()!!.roles!!
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return roles
}


fun userHasRoles(roles: List<String>): Boolean {
    var allowed = false
    try {
        roles.forEach {
            val findRole: UsersRole? = getUserRoles().find { f ->
                f.roleId == it
            }
            if (findRole != null) {
                allowed = true
                return@forEach
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return allowed
}

fun userHasResources(): Boolean {
    var resources = false
    try {
        resources = getUserLogin()?.resources != null
    } catch (_: Exception) {

    }

    return resources
}

fun userHasAllRoles(roles: List<String>): Boolean {
    var allowed = false
    try {
        val userRole = mutableListOf<String?>()
        getUserRoles().forEach {
            userRole.add(it.roleId)
        }
        allowed = userRole.containsAll(roles)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return allowed
}


@Throws(MalformedURLException::class)
fun getBaseUrl(request: HttpServletRequest): String {
    try {
        val requestURL = URL(request.requestURL.toString())
        val port = if (requestURL.port == -1) "" else ":" + requestURL.port
        return requestURL.protocol + "://" + requestURL.host + port + request.contextPath + "/"
    } catch (e: Exception) {
        throw e
    }
}

fun getFullUrl(request: HttpServletRequest): String {
    val requestURL = StringBuilder(request.requestURL.toString())
    val queryString = request.queryString
    return if (queryString == null) {
        requestURL.toString()
    } else {
        requestURL.append('?').append(queryString).toString()
    }
}

fun getUserComponetCodes(): List<String> {
    val rData: MutableList<String> = emptyList<String>().toMutableList()
    try {
        val splitCode = "C"
        val user = getUserLogin()
        user?.roles?.forEach {
            val code = it.role?.code ?: ""
            if (code.contains(splitCode)) {
                val subStr = code.substringAfterLast(splitCode)
                if (subStr != "") {
                    rData.add(subStr)
                }
            }
        }

        if (rData.isEmpty()) {
            rData.add("0")
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return rData
}

fun getEnvFromHttpServletRequest(httpServletRequest: HttpServletRequest, key: String): String {
    var rData = ""
    try {
        val servletContext = httpServletRequest.session.servletContext
        val webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        val env: Environment = webApplicationContext!!.getBean(Environment::class.java)
        rData = env.getProperty(key, "")
        println(rData)
    } catch (_: Exception) {

    }
    return rData
}

@Throws(IOException::class)
fun convertMultiPartToFile(file: MultipartFile): File {
    val convFile = File(file.originalFilename)
    val fos = FileOutputStream(convFile)
    fos.write(file.bytes)
    fos.close()
    return convFile
}

fun getDaysBetween(startDate: Date, endDate: Date, dform: DateFormat): Int {
    return ChronoUnit.DAYS.between(
        LocalDate.parse(dform.format(startDate)),
        LocalDate.parse(dform.format(endDate))
    ).toInt()
}

fun setCompareData(strData: String): Map<String, Any?> {
    val setData = mutableMapOf<String, Any?>()
    val objectMapper = ObjectMapper()

    try {
        val map: Map<String, Any?> = objectMapper.readValue(strData, Map::class.java) as Map<String, Any?>

        val ignoreField = listOf("createdAt", "updatedAt", "createdBy", "createdByUser", "updatedBy", "updatedByUser")

        map.forEach {
            if (!ignoreField.contains(it.key)) {
                when (it.value) {
                    is String -> {
                        setData[it.key] = it.value.toString().trim().lowercase()
                    }
                    is Int -> {
                        setData[it.key] = it.value
                    }
                    is Long -> {
                        setData[it.key] = it.value
                    }
                    is Double -> {
                        setData[it.key] = it.value
                    }
                    is Date -> {
                        setData[it.key] = it.value
                    }
                    is Boolean -> {
                        setData[it.key] = it.value
                    }

                    else -> {
                        try {
                            val parse = objectMapper.writeValueAsString(it.value)
                            if (parse[0] == '[') {
                                try {
                                    val mapValue = it.value as List<Map<String, Any?>>
                                    val mapDatas = mutableListOf<Map<String, Any?>>()
                                    mapValue.forEach { mv ->
                                        mapDatas.add(
                                            setCompareData(
                                                objectMapper.writeValueAsString(mv)
                                            )
                                        )
                                    }
                                    setData[it.key] = mapDatas
                                } catch (_: Exception) {

                                }
                            }
                        } catch (_: Exception) {

                        }
                    }
                }
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return setData
}
