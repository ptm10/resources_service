package id.go.kemenag.madrasah.pmrms.resources.constant

const val EVENT_PROGRESS_REPORT_STATUS_NEW = 0

const val EVENT_PROGRESS_REPORT_STATUS_REVISION = 1

const val EVENT_PROGRESS_REPORT_STATUS_APPROVED_BY_CONSULTAN = 2
