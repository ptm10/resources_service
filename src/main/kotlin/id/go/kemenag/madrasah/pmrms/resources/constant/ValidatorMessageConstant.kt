package id.go.kemenag.madrasah.pmrms.resources.constant

const val VALIDATOR_MSG_REQUIRED = "harus di isi"

const val VALIDATOR_MSG_NOT_VALID = "tidak valid"

const val VALIDATOR_MSG_HAS_USED = "sudah di pakai"

const val VALIDATOR_MSG_HAS_ADDED = "sudah ada di databse"

const val VALIDATOR_MSG_NOT_FOUND = "tidak di temukan"

const val VALIDATOR_MSG_NOT_HAVE_ACCESS = "tidak mempunyai hak akses"

const val VALIDATOR_MSG_DELETE_RELATION = "Data tidak dapat di hapus."

const val VALIDATOR_MSG_REQUEST_FAILED = "Gagal melakukan request data"

const val VALIDATOR_MSG_CANNOT_CHANGE_DATA = "Data tidak dapat di rubah"
