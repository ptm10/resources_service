package id.go.kemenag.madrasah.pmrms.resources.service

import id.go.kemenag.madrasah.pmrms.resources.constant.ROLE_ID_ADMINSTRATOR
import id.go.kemenag.madrasah.pmrms.resources.helpers.getUserLogin
import id.go.kemenag.madrasah.pmrms.resources.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.resources.helpers.userHasRoles
import id.go.kemenag.madrasah.pmrms.resources.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.resources.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.pojo.Iri
import id.go.kemenag.madrasah.pmrms.resources.repository.ComponentRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.IriRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.PdoRepository
import id.go.kemenag.madrasah.pmrms.resources.repository.native2.PdoRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class PdoService {

    @Autowired
    private lateinit var repoNative: PdoRepositoryNative

    @Autowired
    private lateinit var repo: PdoRepository

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoIri: IriRepository

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                val componentCode: String? = getUserLogin()?.component?.code?.substringBefore(".")
                if (componentCode != null) {
                    req.paramIs?.add(ParamSearch("code", "string", componentCode))
                } else {
                    req.paramIs?.add(ParamSearch("code", "string", System.currentTimeMillis().toString()))
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun withIri(): ResponseEntity<ReturnData> {
        return try {
            val datas = mutableListOf<PdoWithIri>()

            repo.findAll().sortedBy { s -> s.code }.forEach {
                datas.add(
                    PdoWithIri(
                        id = it.id,
                        code = it.code,
                        name = it.name,
                        description = it.description,
                        y_2020 = it.y_2020,
                        y_2021 = it.y_2021,
                        y_2022 = it.y_2022,
                        y_2023 = it.y_2023,
                        y_2024 = it.y_2024,
                        iri = repoIri.getByCodeAndActiveOrderByName(it.code).toMutableList()
                    )
                )
            }

            responseSuccess(data = datas)
        } catch (e: Exception) {
            throw e
        }
    }

    fun getByComponentId(id: String?): ResponseEntity<ReturnData> {
        return try {
            val req = Pagination2Request()
            val componentCode: String? = repoComponent.findByIdAndActive(id).get().code?.substringBefore(".")
            if (componentCode != null) {
                req.paramIs?.add(ParamSearch("code", "string", componentCode))
            } else {
                req.paramIs?.add(ParamSearch("code", "string", System.currentTimeMillis().toString()))
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}

data class PdoWithIri(
    var id: String? = null,

    var code: String? = null,

    var name: String? = null,

    var description: String? = null,

    var y_2020: String? = null,

    var y_2021: String? = null,

    var y_2022: String? = null,

    var y_2023: String? = null,

    var y_2024: String? = null,

    var iri: MutableList<Iri> = emptyList<Iri>().toMutableList()
)
