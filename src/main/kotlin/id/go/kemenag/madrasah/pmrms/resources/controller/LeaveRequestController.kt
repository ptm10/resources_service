package id.go.kemenag.madrasah.pmrms.resources.controller

import id.go.kemenag.madrasah.pmrms.resources.model.request.*
import id.go.kemenag.madrasah.pmrms.resources.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.resources.service.LeaveRequestService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Leave Request"], description = "Leave Request API")
@RestController
@RequestMapping(path = ["leave-request"])
class LeaveRequestController {

    @Autowired
    private lateinit var service: LeaveRequestService

    @PostMapping(value = ["datatable-supervisior"], produces = ["application/json"])
    fun datatableSupervisior(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableSupervisior(req)
    }

    @PostMapping(value = ["datatable-staff"], produces = ["application/json"])
    fun datatableStaff(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableStaff(req)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun save(@Valid @RequestBody request: LeaveRequestRequest): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(@PathVariable id: String, @Valid @RequestBody request: LeaveRequestRequest): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PostMapping(value = ["check-date"], produces = ["application/json"])
    fun checkDate(@Valid @RequestBody request: LeaveRequestCheckDateRequest): ResponseEntity<ReturnData> {
        return service.checkDate(request)
    }

    @PutMapping(value = ["approve"], produces = ["application/json"])
    fun approve(@Valid @RequestBody request: ApproveRequest): ResponseEntity<ReturnData> {
        return service.approve(request)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @DeleteMapping(value = [""], produces = ["application/json"])
    fun delete(@Valid @RequestBody request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return service.delete(request)
    }
}
