package id.go.kemenag.madrasah.pmrms.resources.model.users

data class Unit(

    var id: String? = null,

    var name: String? = null
)
