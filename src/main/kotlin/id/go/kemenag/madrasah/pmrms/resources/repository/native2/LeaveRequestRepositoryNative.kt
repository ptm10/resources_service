package id.go.kemenag.madrasah.pmrms.resources.repository.native2

import id.go.kemenag.madrasah.pmrms.resources.pojo.LeaveRequest
import org.springframework.stereotype.Repository

@Repository
class LeaveRequestRepositoryNative : BaseRepositoryNative<LeaveRequest>(LeaveRequest::class.java)
