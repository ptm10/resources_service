FROM openjdk:11.0.13-jre-slim

WORKDIR /opt/app

RUN apt update -y && apt install -y maven

RUN mvn --version

COPY . .
ARG BUILD_ENV=production

RUN cp src/main/resources/application-${BUILD_ENV}.properties src/main/resources/application.properties
RUN mvn package -Dmaven.test.skip=true

RUN mkdir -p /usr/local/pmrms
RUN cp target/pmrms-resources-1.0.0.jar /usr/local/pmrms/pmrms-resources.jar
WORKDIR /usr/local/pmrms
CMD ["java", "-Xmx512M", "-jar", "pmrms-resources.jar"] 
